<?php

namespace App\Listeners;

use App\Events\PaymentCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Utils\SMS;
use App\Models\Eloquent\SMSLogging;
use Session;
use App\Models\Eloquent\Payment;
use Config;
use App\Services\SMSNotification;
class PaymentCreatedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(PaymentCreated $event)
    {
        new SMSNotification($event->payment);
    }
}
