<?php

namespace App\Models\Contracts;

interface AgencyRepositoryInterface
{
    /**
     * Get Agencies by province_id
     * @param integer province_id
     * @return collection|null
     */
    public function getAllByProvinceId($id);
    /**
     * Get Agencies by district_id
     * @param integer district_id
     * @return collection|null
     */
    public function getAllByDistrictId($id);
    /**
     * Get Agencies by ward_id
     * @param integer ward_id
     * @return collection|null
     */
    public function getAllByWardId($id);
    /**
     * Get Agencies by sale_id
     * @param integer sale_id
     * @return collection|null
     */
    public function getAllBySaleId($id);
}
?>