<?php
namespace App\Models\Contracts;

interface RoleRepositoryInterface{
    /**
     * Get role_id by name;
     * @param string $name
     * @return integer $id
     */
    public function getIdByName($name);

}
?>