<?php
namespace App\Models\Contracts;

interface UserRepositoryInterface{
    /**
     * Get all salesman;
     * @return mixed
     */
    public function getAllSalesman();

}
?>