<?php
namespace App\Models\Contracts;

interface RepositoryInterface
{
    /**
    * Get all
    * @return mixed
    **/
    public function getAll();
    /**
     * Get objects with relationship
     * @param array $withRelationShip
     * @return mixed
     */
    public function getAllWithRelationShip(array $withRelationShip);
    /**
     * Get one
     * @param $id
     * @return mixed
    **/
    public function find($id);
    /**
     * Get single object with relationship
     * @param $id
     * @param array $withRelationShip
     * @return mixed
     */
    public function findWithRelationShip($id, array $withRelationShip);
    /**
     * @param array $attributes
     * @return mixed
    **/
    public function create(array $attributes);
    /**
     * Update
     * @param $id
     * @param array $attributes
     * @return mixed
     */
    public function update($id, array $attributes);

    /**
     * Delete
     * @param $id
     * @return mixed
     */
    public function delete($id);

}

?>