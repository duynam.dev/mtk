<?php
namespace App\Models\Repositories;
use App\Models\Contracts\RepositoryInterface;
abstract class BaseEloquentRepository implements RepositoryInterface
{
    /**
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $_model;

    public function __construct()
    {
        $this->setModel();
    }
    /**
     * get model
     * @return string
     */
    abstract public function getModel();
    /**
     * set model
     */
    public function setModel()
    {
        $this->_model = app()->make($this->getModel());
    }
    /**
     * Get all
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll()
    {
        return $this->_model->all();
    }
    /**
     * Get all with relationship
     * @param string|array
     * @return mixed
     */
    public function getAllWithRelationShip($withRelationship)
    {
        return $this->_model->with($withRelationship)->get();
    }
    /**
     * Get one
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        $result = $this->_model->find($id);

        return $result;
    }
    public function findWithRelationShip($id, array $withRelationship)
    {
         return $result = $this->_model->where('id',$id)->with($withRelationship)->first();
    }
    /**
     * Create
     * @param array $attributes
     * @return mixed;
     */
    public function create(array $attributes)
    {
        return $this->_model->create($attributes);
    }
    /**
     * Update
     * @param $id
     * @param array $attributes
     * @return bool|mixed
     */
    public function update($id, array $attributes)
    {
        $result = $this->find($id);
        if($result)
        {
            $result->update($attributes);
            return $result;
        }
        return false;
    }
    /**
     * Delete
     * @param $id
     * @return bool
     */
    public function delete($id)
    {
        $result = $this->find($id);
        if($result)
        {
            $result->delete();
            return true;
        }
        return false;
    }
}
?>