<?php

namespace App\Models\Repositories;
use DB;
use App\Models\Repositories\BaseEloquentRepository;
use App\Models\Contracts\RoleRepositoryInterface;
use App\Models\Contracts\SalesmanRepositoryInterface;
class SalesmanEloquentRepository extends BaseEloquentRepository implements SalesmanRepositoryInterface
{
    /**
     * @var App\Models\Repositories\RoleEloquentRepository
     */
    private $roleRepository;
    private $sales_role_id;
    public function __construct(RoleRepositoryInterface $roleRepository)
    {
        parent::__construct();
        $this->roleRepository = $roleRepository;
        $sales_role_id = $roleRepository->getIdByName('sale');
    }

    public function getModel()
    {
        return \App\Models\Eloquent\User::class;
    }
    /**
     * Get all salesman from users table
     * @return mixed
     */
    public function getAll()
    {
        //DB::enableQueryLog();
        $data = $this->_model->whereHas('roles', function($q){
            $q->where('name','sale');
        })->withCount(['agencies','payments'])->get();

        return $data;
    }
    /**
     * Save salesman to users table
     * @param array $attributes
     * @return mixed
     */
    public function create(array $attributes)
    {
        $saved = $this->_model->create($attributes);
        $saved->roles()->attach($this->sales_role_id);
        return $saved;
    }
    /**
     * Find the salesman by id
     * @param integer id
     * @return mixed
     */
    public function find($id)
    {
        return $this->_model->withCount('agencies')->find($id);
    }
}
?>