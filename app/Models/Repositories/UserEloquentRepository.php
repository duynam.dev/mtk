<?php
namespace App\Models\Repositories;

use App\Models\Repositories\BaseEloquentRepository;
use App\Models\Contracts\UserRepositoryInterface;

class UserEloquentRepository extends BaseEloquentRepository implements UserRepositoryInterface
{
    /**
     * Get model
     * @return string
     */
    public function getModel()
    {
        return \App\Models\Eloquent\User::class;
    }
    public function getAllSalesman()
    {
        return $this->_model->with(['roles' => function($q){
            $q->where('name', 'sale');
        }])->get();
    }
}
?>