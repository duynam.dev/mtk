<?php
namespace App\Models\Repositories;

use App\Models\Repositories\BaseEloquentRepository;
use App\Models\Contracts\AgencyRepositoryInterface;

class AgencyEloquentRepository extends BaseEloquentRepository implements AgencyRepositoryInterface
{
    
    public function getModel()
    {
        return \App\Models\Eloquent\Agency::class;
    }
    public function find($id)
    {
        $result = $this->_model->with("customers")->find($id);

        return $result;
    }
     /**
     * Get Agencies by province_id
     * @param integer province_id
     * @return collection|null
     */
    public function getAllByProvinceId($province_id)
    {
        return $this->_model->where('province_id',$province_id)->get();
    }
    /**
     * Get Agencies by district_id
     * @param integer district_id
     * @return collection|null
     */
    public function getAllByDistrictId($district_id)
    {
        return $this->_model->where('province_id',$district_id)->get();
    }
    /**
     * Get Agencies by ward_id
     * @param integer ward_id
     * @return collection|null
     */
    public function getAllByWardId($ward_id)
    {
        return $this->_model->where('province_id',$ward_id)->get();
    }
    /**
     * Get Agencies by sale_id
     * @param integer sale_id
     * @return collection|null
     */
    public function getAllBySaleId($sale_id)
    {
        return $this->_model->where('sale_id',$sale_id)->with('ward','district','province')->get();
    }
}
?>