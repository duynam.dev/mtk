<?php
namespace App\Models\Repositories;

use App\Models\Repositories\BaseEloquentRepository;
use App\Models\Contracts\RoleRepositoryInterface;

class RoleEloquentRepository extends BaseEloquentRepository implements RoleRepositoryInterface
{
    /**
     * Get model
     * @return $model
     */
    public function getModel()
    {
        return \App\Models\Eloquent\Role::class;
    }
    /**
     * Get role_id by name
     * @param string name
     * @return integer $id
     */
    public function getIdByName($name)
    {
        return $this->_model->where('name',$name)->first()->id;
    }
}
?>