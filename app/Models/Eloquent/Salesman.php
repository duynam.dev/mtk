<?php

namespace App\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

class Salesman extends User
{
    protected $table = "users";
    public static function all($columns = ['*']){
        return User::with('role','agencies')->whereHas('role',function($query){
            $query->where('name','sale');
        })->get();
    }

}
