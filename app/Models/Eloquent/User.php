<?php

namespace App\Models\Eloquent;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Config;
use DB;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email','phone', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    protected $appends = ['status_label','revenue','is_admin'];
    public function roles()
    {
        return $this->belongsToMany('App\Models\Eloquent\Role');
    }
    public function agencies()
    {
        return $this->hasMany('App\Models\Eloquent\Agency','sale_id');
    }
    public function payments()
    {
        return $this->hasMany('App\Models\Eloquent\Payment');
    }
    public function scopeSalesman($query)
    {
        return $query->whereHas('roles', function($roles){
            $roles->where('name','sale');
        });
    }
    public function getStatusLabelAttribute()
    {
        switch($this->status)
        {
            case Config::get('constants.user_status.active'):
                return '<label class="badge badge-success">Active</label>';
            case Config::get('constants.user_status.banned'):
                return '<label class="badge badge-danger">Banned</label>';
            case Config::get('constants.user_status.inactive'):
                return '<label class="badge badge-secondary">Inactive</label>';
            default:
                return '<label class="badge badge-default">Unknown!</label>';
        }
    }
    public function getRevenueAttribute()
    {
        return $this->payments()->sum(DB::raw('amount'));
    }
    public function isAdmin(){
        foreach ($this->roles()->get() as $role)
        {
            //var_dump($this->roles()->get());
            if ($role->name == 'admin')
            {
                return true;
            }
        }

        return false;
    }
    public function getIsAdminAttribute(){
        foreach ($this->roles()->get() as $role)
        {
            //var_dump($this->roles()->get());
            if ($role->name == 'admin')
            {
                return 1;
            }
        }

        return 0;
    }

}
