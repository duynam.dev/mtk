<?php

namespace App\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = 'provinces';
    public $timestamps = false;

    public function districts()
    {
        return $this->hasMany(\App\Models\Eloquent\District::class);
    }
}
