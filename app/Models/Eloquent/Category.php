<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $table = 'category';
	protected $appends = ['item_quantity'];

    public function items()
    {
    	$this->hasMany('App\Item');
    }
    public function getItemQuantityAttribute(){
    	return Item::where('category_id',$this->getKey())->count();
    }
}
