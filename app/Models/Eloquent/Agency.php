<?php

namespace App\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;
use DB;

class Agency extends Model
{
    protected $table = 'agency';
    protected $fillable = ['name','address','sale_id'];

    protected $appends = ['revenue','owner'];

    public function customers()
    {
        return $this->hasMany('App\Models\Eloquent\Customer');
    }
    public function ward()
    {
        return $this->belongsTo('App\Models\Eloquent\Ward');
    }
    public function district()
    {
        return $this->belongsTo('App\Models\Eloquent\District');
    }
    public function province()
    {
        return $this->belongsTo('App\Models\Eloquent\Province');
    }
    public function salesman()
    { 
        return $this->belongsTo('App\Models\Eloquent\User','sale_id');
    }
    public function payments()
    {
        return $this->hasMany('App\Models\Eloquent\Payment');
    }
    public function getRevenueAttribute()
    {
        return $this->payments()->sum(DB::raw('amount'));
    }
    public function getOwnerAttribute()
    {
        return $this->customers()->where('is_owner',1)->first();
    }
}
