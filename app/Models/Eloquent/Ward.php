<?php

namespace App\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

class Ward extends Model
{
    protected $table = 'wards';

    public function district()
    {
        return $this->belongsTo(\App\Models\Eloquent\District::class);
    }
}
