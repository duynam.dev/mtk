<?php

namespace App\Models\Eloquent;

use Config;
use DB;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    public $timestamps = true;
    protected $fillable = ['type', 'user_id', 'created_at', 'amount', 'agency_id', 'customer_id'];
    protected $table = 'payments';

    //salesman_paid: Số tiền mà Sale đã trả về cho công ty từ Giao dịch này, không tìm được từ Tiếng Anh thích hợp...

    protected $appends = ['status_label', 'salesman_paid', 'payment_method'];

    public function order()
    {
        return $this->belongsTo(\App\Models\Eloquent\Order::class);
    }
    public function customer()
    {
        return $this->belongsTo(\App\Models\Eloquent\Customer::class);
    }
    public function agency()
    {
        return $this->belongsTo(\App\Models\Eloquent\Agency::class);
    }
    public function paymentsToCompanyDetails()
    {
        return $this->hasMany(\App\Models\Eloquent\PaymentsToCompanyDetail::class);
    }
    // Person who create/receive payment
    public function user()
    {
        return $this->belongsTo(\App\Models\Eloquent\User::class);
    }
    public function salesman()
    {
        return $this->belongsTo(\App\Models\Eloquent\User::class, 'user_receive_id');
    }
    public function getPaymentMethodAttribute($value)
    {
        return array_search($value, Config::get('constants.payment_method'));
    }
    public function getSalesmanPaidAttribute()
    {
        return $this->paymentsToCompanyDetails()->sum(DB::raw('amount'));
    }
    public function getStatusLabelAttribute()
    {
        switch ($this->status) {
            case Config::get('constants.payment_status.pending'):
                return '<span class="badge badge-secondary">Chờ xử lý</span>';
            case Config::get('constants.payment_status.processing'):
                return '<span class="badge badge-secondary">Đang xử lý</span>';
            case Config::get('constants.payment_status.sms_not_sent'):
                return '<span class="badge badge-warning">Chưa gửi SMS</span>';
            case Config::get('constants.payment_status.completed'):
                return '<span class="badge badge-success">Hoàn thành</span>';
            case Config::get('constants.payment_status.cancelled'):
                return '<span class="badge badge-danger">Đã hủy</span>';
            default:
                return '<span class="badge badge-default">Unknown</span>';
        }
    }
    public function updateStatus()
    {
        if ($this->amount == $this->salesman_paid) {
            $this->status = Config::get('constants.payment_status.completed');
            $this->confirmed_at = date('Y-m-d H:i:s');
        }
        if ($this->amount == 0) {
            $this->status = Config . get('constants.payment_status.pending');
            $this->confirmed_at = null;
        }
        if ($this->salesman_paid > 0 && $this->salesman_paid < $this->amount) {
            $this->status = Config . get('constants.payment_status.processing');
            $this->confirmed_at = null;
        }
    }
    public function getCountByDays($from_date = '1970-01-01 00:00:00', $to_date = null)
    {
        $now = date('Y-m-d H:i:s');
        if ($to_date == null) {
            $to_date = $now;
        }
        $countPaymentsByDays = DB::table($this->table)->select(DB::raw('DATE(`created_at`) as date, count(id) as num'))
            ->whereBetween('created_at', [$from_date, $to_date])
            ->groupBy('date')
            ->orderBy('date')
            ->get();
        return $countPaymentsByDays;

    }
    public function getSumAmountByDays($from_date = '1970-01-01 00:00:00', $to_date = null)
    {
        $now = date('Y-m-d H:i:s');
        if ($to_date == null) {
            $to_date = $now;
        }
        //  DB::enableQueryLog();
        $sumAmountByDays = DB::table($this->table)->select(DB::raw('DATE(`created_at`) as date, SUM(amount) as amount'))
            ->whereBetween('created_at', [$from_date, $to_date])
            ->groupBy('date')
            ->orderBy('date')
            ->get();

        return $sumAmountByDays;
    }
}
