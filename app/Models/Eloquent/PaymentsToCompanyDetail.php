<?php

namespace App\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

class PaymentsToCompanyDetail extends Model
{
    protected $table = 'payments_to_company_details';
  

    public function payment()
    {
        return $this->belongsto(\App\Models\Eloquent\Payment::class);
    }
}
