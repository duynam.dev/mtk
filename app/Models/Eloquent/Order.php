<?php

namespace App\Models\Eloquent;

use Config;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $appends = [ 'status_label', 'amount' ];

    public function item()
    {
    	return $this->belongsTo('App\Models\Eloquent\Item');
    }
    public function customer()
    {
    	return $this->belongsTo('App\Models\Eloquent\Customer');
    }
    public function payments()
    {
    	return $this->hasMany('App\Models\Eloquent\Payment');
    }
    public function getAmountAttribute(){
    	return $this->quantity*($this->item->price);
    }
    public function getStatusAttribute($value)
    {
        return array_search($value, Config::get('constants.order_status'));
    }
    public function getStatusLabelAttribute()
    {
        switch ($this->status) {
            case 'pending':
                return 'warning';
                break;
            case 'processing':
                return 'info';
                break;
            case 'completed':
                return 'success';
                break;
            case 'canceled':
                return 'default';
                break;
            default:
                return 'default';
                break;
        }
    }
}
