<?php

namespace App\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;


class SMSLogging extends Model
{
    protected $table = 'sms_log';
    protected $appends = ['status_label'];
    protected $fillable = ['payment_id','customer_id','pin_code','content','status','error','error_code'];

    public function customer()
    {
        return $this->belongsTo('App\Models\Eloquent\Customer');
    }
    public function payment()
    {
        return $this->belongsTo('App\Models\Eloquent\Payment');
    }
    public function getStatusLabelAttribute()
    {
        if($this->status == 0)
        {
            return '<span class="badge badge-danger">Không gửi được</span>';
        }
        return '<span class="badge badge-success">Thành công</span>';
        
    }
}
