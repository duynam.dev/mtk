<?php

namespace App\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $table = 'districts';

    public function wards()
    {
        return $this->hasMany(\App\Models\Eloquent\Ward::class);
    }
    public function province()
    {
        return $this->belongsTo(\App\Models\Eloquent\Province::class);
    }
}
