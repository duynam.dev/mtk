<?php

namespace App\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    public $fillable = ['fullname','gender','email','phone_number'];
    public function Agency()
    {
        return $this->belongsTo(\App\Models\Eloquent\Agency::class);
    }
}
