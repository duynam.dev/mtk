<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use App\Models\Contracts\UserRepositoryInterface;
use App\Models\Repositories\UserEloquentRepository;
use App\Models\Contracts\SalesmanRepositoryInterface;
use App\Models\Repositories\SalesmanEloquentRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    public function boot(){

    }
    public function register()
    {
          $models = array(
            'User',
            'Salesman',
            'Role',
            'Agency'
        );

        foreach ($models as $model) {
            $this->app->bind("App\Models\Contracts\\{$model}RepositoryInterface", "App\Models\Repositories\\{$model}EloquentRepository");
        }
        //  $this->app->singleton(UserRepositoryInterface::class, UserEloquentRepository::class);
        //  $this->app->singleton(SalesmanRepositoryInterface::class, SalesmanEloquentRepository::class);
        //  $this->app->singleton(RoleRepositoryInterface::class, RoleEloquentRepository::class);
    }
}
?>