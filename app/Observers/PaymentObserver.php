<?php

namespace App\Observers;

use App\Models\Eloquent\Payment;
use App\Events\PaymentCreated;
use App\Events\PaymentUpdated;

class PaymentObserver
{
    /**
     * Handle the payment "created" event.
     *
     * @param  \App\Models\Eloquent\Payment  $payment
     * @return void
     */
    public function created(Payment $payment)
    {
        broadcast(new PaymentCreated($payment));
    }

    /**
     * Handle the payment "updated" event.
     *
     * @param  \App\Models\Eloquent\Payment  $payment
     * @return void
     */
    public function updated(Payment $payment)
    {
        broadcast(new PaymentUpdated($payment));
    }

    /**
     * Handle the payment "deleted" event.
     *
     * @param  \App\Models\Eloquent\Payment  $payment
     * @return void
     */
    public function deleted(Payment $payment)
    {
        //
    }

    /**
     * Handle the payment "restored" event.
     *
     * @param  \App\Models\Eloquent\Payment  $payment
     * @return void
     */
    public function restored(Payment $payment)
    {
        //
    }

    /**
     * Handle the payment "force deleted" event.
     *
     * @param  \App\Models\Eloquent\Payment  $payment
     * @return void
     */
    public function forceDeleted(Payment $payment)
    {
        //
    }
}
