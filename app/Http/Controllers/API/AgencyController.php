<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contracts\AgencyRepositoryInterface;
use App\Models\Eloquent\Agency;
use App\Models\Eloquent\Customer;

class AgencyController extends Controller
{
    private $agencyRepository;
    public function __construct(AgencyRepositoryInterface $agencyRepository)
    {
        $this->agencyRepository = $agencyRepository;
    }
    public function getAll(Request $request)
    {
        if($request->user()->is_admin){
            $agencies = $this->agencyRepository->getAllWithRelationShip(['province','district','ward','customers','salesman']);
        }else{
            $agencies = Agency::with(['province','district','ward','customers','salesman'])->where('sale_id',$request->user()->id)->get();
        }
        
        return response()->json(['data' => $agencies]);
    }
    public function getAgenciesNotHaveSale(){
        $agencies = Agency::where('sale_id',null)->with(['province','district','ward','customers'])->get();
        return response()->json(['status' => true,'data' => $agencies]);
    }
    public function getAllBySaleId(Request $request)
    {
        $sale_id = $request->sale_id;
        $agencies = $this->agencyRepository->getAllBySaleId($sale_id);
        return response()->json(['data' => $agencies], 200);
    }
    public function getById(Request $request)
    {
        $agency = Agency::with(['salesman','province','district','ward','customers'])->withCount('payments')->find($request->id);
        return response()->json($agency);
    }
    public function delete(Request $request){
        $id = $request->id;
        try{
            $agency  = Agency::find($id);
            $agency->delete();
            return response()->json(['status'=>true,'msg' => 'Đã xóa thành công']);
        }catch(\Exception $e){
            return response()->json(['status'=>false,'msg' => 'Lỗi: '.$e->getMessage()]);
        }
        
    }
    public function store(Request $request)
    {
        $posted_agency = $request->agency;
        $customer = $request->customer;
        $owner = Customer::create($customer);
        $agency = new Agency();
        $agency->name = $posted_agency['name'];
        $agency->province_id = $posted_agency['province_id'];
        $agency->district_id = $posted_agency['district_id'];
        $agency->ward_id = $posted_agency['ward_id'];
        $agency->address = $posted_agency['address'];
        $agency->sale_id = $posted_agency['sale_id'];
        try{
            $agency->save();
            $owner->agency_id = $agency->id;
            $owner->is_owner = 1;
            $owner->update();
            return response()->json(['status' => true, 'msg' => 'Cập nhật thành công!', 'data' => $agency]);   
        }catch(\Exception $e){
            return response()->json(['status' => false, 'msg' => 'Cập nhật thật bại! \n'.$e->getMessage(), 'data' => $agency]); 
        }
        
    }
    public function update(Request $request)
    {
        $posted_agency = $request->agency;
        $customer = $request->customer;
       
        $isNewCustomer = false;
        if($customer['id'] !== '' && !empty($customer['id']))
        {
            $owner = Customer::find($customer['id']);
            
        }else{
            $isNewCustomer = true;
            $owner = new Customer();
            $owner->is_owner = 1;
            $owner->agency_id = $posted_agency['id'];
        }
        $owner->fullname = $customer['fullname'];
        $owner->email = $customer['email'];
        $owner->phone_number = $customer['phone_number'];
        $owner->gender = $customer['gender'];
        $agency = Agency::find($posted_agency['id']);
        $agency->name = $posted_agency['name'];
        $agency->province_id = $posted_agency['province_id'];
        $agency->district_id = $posted_agency['district_id'];
        $agency->ward_id = $posted_agency['ward_id'];
        $agency->address = $posted_agency['address'];
        $agency->sale_id = $posted_agency['sale_id'];
        try{
            $agency->update();
            if($isNewCustomer)
            {
                $owner->save();
            }else{
                $owner->update();
            }
            
            return response()->json(['status' => true, 'msg' => 'Cập nhật thành công!', 'data' => $agency]);   
        }catch(\Exception $e){
            return response()->json(['status' => false, 'msg' => 'Cập nhật thật bại! \n'.$e->getMessage(), 'data' => $agency]); 
        }
        
    }
}
