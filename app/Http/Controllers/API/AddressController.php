<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Eloquent\Province;
use App\Models\Eloquent\District;
use App\Models\Eloquent\Ward;

class AddressController extends Controller
{
    public function provinces()
    {
        $provinces = Province::orderBy('name')->get();
        return response()->json($provinces);
    }
    public function getDistrictsByProvinceId(Request $request)
    {
        $province_id = $request->province_id;
        $districts = District::where('province_id',$province_id)->orderBy('name')->get();
        return response()->json($districts);
    }
    public function getWardsByDistrictId(Request $request)
    {
        $district_id = $request->district_id;
        $wards = Ward::where('district_id',$district_id)->orderBy('name')->get();
        return response()->json($wards);
    }
}
