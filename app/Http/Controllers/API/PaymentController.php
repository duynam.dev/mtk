<?php

namespace App\Http\Controllers\API;

use App\Events\PaymentUpdated;
use App\Http\Controllers\Controller;
use App\Models\Eloquent\Agency;
use App\Models\Eloquent\Payment;
use App\Models\Eloquent\PaymentsToCompanyDetail as SalesmanPayment;
use App\Services\SMSNotification;
use Carbon\Carbon;
use Config;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function getLive()
    {
        $payments = Payment::with(['salesman', 'agency', 'customer', 'user', 'paymentsToCompanyDetails'])->where('created_at', '>=', Carbon::today())->orWhere('confirmed_at', null)->get();
        return $payments;
    }
    public function getAll(Request $request)
    {
        if ($request->user()->is_admin) {
            $payments = Payment::with(['salesman', 'agency', 'customer', 'user', 'paymentsToCompanyDetails'])->get();
        } else {
            $payments = Payment::with(['salesman', 'agency', 'customer', 'user', 'paymentsToCompanyDetails'])->where('user_receive_id', $request->user()->id)->get();
        }
        return $payments;
    }
    public function confirm(Request $request)
    {
        $id = $request->id;
        $reference_code = $request->reference_code;
        $now = date('Y-m-d H:i:s');
        $payment = Payment::with(['paymentsToCompanyDetails'])->find($id);
        $amount = str_replace(',', '', $request->amount);
        $salesmanPayment = new SalesmanPayment();
        if ($payment->confirmed_at != null) {
            return response()->json(['status' => false, 'msg' => 'Giao dịch đã được xác nhận trước đó', 'data' => null]);
        }

        if ($payment->amount < ($amount + $payment->salesman_paid)) {
            return response()->json(['status' => false, 'msg' => 'Tổng số tiền lớn hơn số tiền của giao dịch', 'data' => null]);
        }
        $salesmanPayment->note = $request->note;
        $salesmanPayment->amount = $amount;
        $salesmanPayment->reference_code = $reference_code;
        $salesmanPayment->user_id = $request->user()->id;
        $salesmanPayment->payment_id = $id;

        try {
            $salesmanPayment->save();
            //Retrive $payment to update salesman_paid
            $payment = Payment::with(['salesman', 'agency', 'customer', 'user', 'paymentsToCompanyDetails'])->find($id);
            //  $salesman_paid = $salesmanPayment->amount + $payment->salesman_paid;
            if ($payment->amount == $payment->salesman_paid) {

                $payment->confirmed_at = $now;
                $payment->status = Config::get('constants.payment_status.completed');
                $payment->update();
            } elseif ($payment->status !== Config::get('constants.payment_status.processing')) {
                // check the payments status if it's not in processing, set it to this status.
                $payment->status = Config::get('constants.payment_status.processing');
                $payment->update();
            } else {
                //Broadcast PaymentUpdated to tell the client update new payments history data
                broadcast(new PaymentUpdated($payment));
            }
            return response()->json(['status' => true, 'msg' => 'Giao dịch đã được cập nhật', 'data' => $payment]);

        } catch (\Exception $e) {
            return response()->json(['status' => false, 'msg' => 'Không cập nhật được giao dịch \n' . $e->getMessage(), 'data' => null]);
        }

    }
    public function store(Request $request)
    {
        $payment = new Payment();
        $payment->customer_id = $request->customer_id;
        $payment->type = $request->payment_method;
        $payment->amount = str_replace(',', '', $request->amount);
        $payment->agency_id = $request->agency_id;
        $payment->status = Config::get('constants.payment_status.pending');
        $payment->user_id = $request->user()->id;
        if ($request->has('user_receive_id')) {
            $payment->user_receive_id = $request->user_receive_id;
        } else {
            $payment->user_receive_id = Agency::find($request->agency_id)->sale_id;
        }
        $payment->save();
        return response()->json(['status' => true, 'data' => $payment]);

    }
    public function deletePaymentDetail(Request $request)
    {
        try {
            $paymentDetailId = $request->detail_id;

            //SalesmanPayment mean the PaymentsToCompanyDetail, sorry for this sh*t
            $salesmanPayment = SalesmanPayment::find($paymentDetailId);
            $payment_id = $salesmanPayment->payment_id;
            $salesmanPayment->delete();
            $payment = Payment::with(['paymentsToCompanyDetails'])->find($payment_id);
            $payment = $this->updateStatus($payment);
            $payment->update();
            broadcast(new PaymentUpdated($payment));
            return response()->json(['status' => true, 'msg' => 'Giao dịch đã được cập nhật', 'data' => $payment]);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'msg' => 'Lỗi :' . $e->getMessage(), 'data' => null]);
        }

    }
    public function editPaymentDetail(Request $request)
    {
        $paymentDetail = SalesmanPayment::find($request->detail_id);
        $reference_code = $request->reference_code;
        $now = date('Y-m-d H:i:s');
        $payment = Payment::with(['paymentsToCompanyDetails'])->find($paymentDetail->payment_id);
        $amount = str_replace(',', '', $request->amount);

        // Số tiền mới của giao dịch = tổng số tiền cũ - số tiền của giao dịchnộp cũ + số tiền của giao dịch nộp sau khi chỉnh sửa
        if ($payment->amount < ($amount + ($payment->salesman_paid - $paymentDetail->amount))) {
            return response()->json(['status' => false, 'msg' => 'Tổng số tiền lớn hơn số tiền của giao dịch', 'data' => null]);
        }
        $paymentDetail->note = $request->note;
        $paymentDetail->amount = $amount;
        $paymentDetail->reference_code = $reference_code;
        $paymentDetail->user_id = $request->user()->id;

        try {
            $paymentDetail->update();
            //Retrive $payment to update salesman_paid
            $payment = Payment::with(['salesman', 'agency', 'customer', 'user', 'paymentsToCompanyDetails'])->find($paymentDetail->payment_id);
            $payment = $this->updateStatus($payment);
            $payment->update();
            //Broadcast PaymentUpdated to tell the client update new payments history data
            broadcast(new PaymentUpdated($payment));

            return response()->json(['status' => true, 'msg' => 'Giao dịch đã được cập nhật', 'data' => $payment]);

        } catch (\Exception $e) {
            return response()->json(['status' => false, 'msg' => 'Không cập nhật được giao dịch /n' . $e->getMessage(), 'data' => null]);
        }
    }
    private function updateStatus($payment)
    {
        if ($payment->amount == $payment->salesman_paid) {
            $payment->status = Config::get('constants.payment_status.completed');
            $payment->confirmed_at = date('Y-m-d H:i:s');
            return $payment;
        }
        if ($payment->salesman_paid == 0) {
            $payment->status = Config::get('constants.payment_status.pending');
            $payment->confirmed_at = null;
            return $payment;
        }
        if ($payment->salesman_paid > 0 && $payment->salesman_paid < $payment->amount) {
            $payment->status = Config::get('constants.payment_status.processing');
            $payment->confirmed_at = null;
            return $payment;
        }
    }
    public function update(Request $request)
    {
        $payment = Payment::with(['salesman', 'agency', 'customer', 'user', 'paymentsToCompanyDetails'])->find($request->id);

        $payment->amount = str_replace(',', '', $request->amount);
        $payment->type = $request->payment_method;
        try {
            $payment->update();
            if ($request->resendSMS) {
                $smsNoti = new SMSNotification($payment, true);
                $smsNoti->sendSMS();
            }
            //  broadcast(new PaymentUpdated($payment));
            return response()->json(['status' => true, 'msg' => 'Giao dịch đã được cập nhật', 'data' => $payment]);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'msg' => 'Không cập nhật được giao dịch /n' . $e->getMessage(), 'data' => null]);
        }

    }
    public function destroy(Request $request)
    {
        $payment = Payment::find($request->id);
        try {
            $salesmanPayments = SalesmanPayment::where('payment_id', $payment->id)->get();
            if (!empty($result)) {
                foreach ($salesmanPayments as $salemanPayment) {
                    $salemanPayment->delete();
                }
            }
            $payment->delete();
            return response()->json(['status' => true, 'msg' => 'Đã xóa thành công'], 200);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'msg' => 'Thao tác không thành công! Lỗi: ' . $e->getMessage()], 500);

        }
    }
    // Get data for main chart on dashboard
    // I really confused that should I only return the value without date in function in Payment Class
    // but I think maybe I'll need this data with date in somewhere, so I coded a shit function like this....
    public function getCountPaymentByDays()
    {

        $payment = new Payment();
        $paymentCountByDays = $payment->getCountByDays()->toArray();
        $paymentSumAmountByDays = $payment->getSumAmountByDays()->toArray();

        //this is that shit
        $date_labels = array_map(function ($value) {
            return date_format(date_create($value->date), "d/m");
        }, $paymentSumAmountByDays);

        $countByDays = array_map(function ($value) {
            return $value->num;
        }, $paymentCountByDays);

        $sumAmountByDays = array_map(function ($value) {
            return $value->amount;
        }, $paymentSumAmountByDays);
        $data = ['labels' => $date_labels, 'countByDays' => $countByDays, 'sumAmountByDays' => $sumAmountByDays];
        //  var_dump($paymentSumAmountByDays);
        return $data;
    }
}
