<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Eloquent\Role;

class RoleController extends Controller
{
    public function getAll()
    {
        $roles = Role::all();
        return $roles;
    }
}
