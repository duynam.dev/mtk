<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Eloquent\Agency;
use App\Models\Eloquent\Payment;
use App\Models\Eloquent\User;
use DB;

class DashboardController extends Controller
{
    const SALE_ROLE_ID = 1;
    public function getSummary()
    {
        $countProvinces = Agency::count(DB::raw('DISTINCT province_id'));
        $countAgencies = Agency::count();
        $countSalesmen = DB::table('role_user')->where('role_id',self::SALE_ROLE_ID)->count('user_id');
        $countPayments = Payment::count();
        $summary = array(
            'countProvinces' => $countProvinces,
            'countAgencies' => $countAgencies,
            'countSalesmen' => $countSalesmen,
            'countPayments' => $countPayments
        );
        return $summary;
    }
}
