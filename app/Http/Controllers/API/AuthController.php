<?php

namespace App\Http\Controllers\API;

use Exception;
use Illuminate\Http\JsonResponse;
use JWTAuth;
use Log;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\UserLoginRequest;
use Illuminate\Http\Request;
use Config;

class AuthController extends Controller
{
    /**
     * Log a user in.
     *
     * @param UserLoginRequest $request
     *
     * @return JsonResponse
     */
    public function login(UserLoginRequest $request)
    {
        try {
            if (!$token = JWTAuth::attempt($request->only('email', 'password'))) {
                return response()->json(['error' => 'invalid_credentials', 'message' => 'Tài khoản không tồn tại trên hệ thống'], 401);
            }
        } catch (JWTException $e) {
            Log::error($e);

            return response()->json(['error' => 'could_not_create_token', 'message' => 'Không thể tạo phiên làm việc'], 500);
        }
        $user = JWTAuth::toUser($token);
        $banned_status =  Config::get('constants.user_status.banned');
        if($user->status == $banned_status){
            return response()->json(['error' => 'banned_user', 'message' => 'Tài khoản đã bị khóa. Liên hệ Quản trị viên để biết thêm chi tiết.'],401);
        }
        return response()->json(['token'=>$token,'user'=>$request->user()]);
    }

    /**
     * Log the current user out.
     *
     * @return JsonResponse
     */
    public function logout()
    {
        if ($token = JWTAuth::getToken()) {
            try {
                JWTAuth::invalidate($token);
            } catch (Exception $e) {
                Log::error($e);
            }
        }

        return response()->json();
    }
    public function get(Request $request)
    {
        return response()->json($request->user());
    }
}
