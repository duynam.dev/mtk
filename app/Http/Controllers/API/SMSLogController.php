<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Eloquent\SMSLogging;
use App\Models\Eloquent\Payment;
use App\Utils\SMS;

class SMSLogController extends Controller
{
    public function getAll()
    {
        $sms_logs = SMSLogging::with('payment')->orderBy('created_at','desc')->get();
        return $sms_logs;
    }
    public function getAccountInfo()
    {
        $sms_utils = new SMS();
        $info = $sms_utils->getUserInfo();
        return $info;
    }
}
