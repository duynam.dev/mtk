<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contracts\AgencyRepositoryInterface;
use App\Models\Contracts\SalesmanRepositoryInterface;
use App\Models\Eloquent\Agency;
use App\Models\Eloquent\Payment;
use App\Models\Eloquent\User;
use Config;

class SalesmanController extends Controller
{
    private $salesmanRepository;
    private $agencyRepository;

    public function __construct(AgencyRepositoryInterface $agencyRepository, SalesmanRepositoryInterface $salesmanRepository)
    {
        $this->salesmanRepository = $salesmanRepository;
        $this->agencyRepository = $agencyRepository;
    }
    public function get()
    {
        $salesmen = User::salesman()->withCount(['agencies','payments'])->get();
        return $salesmen;
    }
    public function getById(Request $request)
    {
        $id = $request->id;
        $salesman = User::salesman()->with(['agencies.province','agencies.district','agencies.ward','roles'])->find($id);
        return response()->json($salesman);
    }
    public function getAgencies(Request $request,$salesman_id)
    {
        $agencies = $this->agencyRepository->getAllBySaleId($salesman_id);
        return response(['data' => $agencies], 200);
    }
    public function update(Request $request)
    {
        $posted_salesman = $request->salesman;
        $salesman = User::findOrFail($posted_salesman['id']);

        $salesman->name = $posted_salesman['name'];
        $salesman->email = $posted_salesman['email'];
        $salesman->phone =$posted_salesman['phone'];
        try{
            $salesman->update();
            return response()->json(['status' => true, 'msg' => 'Cập nhật thành công!', 'data' => $salesman]);
        }catch(\Exception $e)
        {
            return response()->json(['status' => false, 'msg' => 'Cập nhật thất bại. Lỗi: '.$e->getMessages(), 'data' => $salesman]);
        }
    }
    public function addAgencies(Request $request){
        $agency_ids = $request->agency_ids;
        $sale_id = $request->sale_id;
        $saved = Agency::whereIn('id',$agency_ids)->update(['sale_id' => $sale_id]);
        return response($saved);
    }
    public function deleteAgency(Request $request)
    {
        $sale_id = $request->sale_id;
        $agency_id = $request->agency_id;
        $saved = Agency::where('id',$agency_id)->update(['sale_id' => null]);
            return response()->json(['status' => $saved]);
        
    }
    public function syncAgencies(Request $request)
    {
        $sale_id = $request->sale_id;
        $newAgencies = $request->new_agencies;
        $noSaleAgencies = $request->no_sale_agencies;
        try{
            foreach($newAgencies as $agency)
            {
                $db_agency = Agency::findOrFail($agency['id']);
                $db_agency->sale_id = $sale_id;
                $db_agency->update();
            }
            foreach($noSaleAgencies as $agency)
            {
                $db_agency = Agency::findOrFail($agency['id']);
                $db_agency->sale_id = null;
                $db_agency->update();
            }
            $salesman = User::salesman()->with(['agencies.province','agencies.district','agencies.ward','roles'])->find($sale_id);
            return response()->json(['status' => true, 'msg' => 'Cập nhật thành công', 'data'=>$salesman]);
        }catch(\Exception $e){
            $salesman = User::salesman()->with(['agencies.province','agencies.district','agencies.ward','roles'])->find($sale_id);
            return response()->json(['status' => true, 'msg' => 'Cập nhật thất bại. Lỗi: '.$e->getMessage(), 'data'=>$id]);
        }

    }

}
