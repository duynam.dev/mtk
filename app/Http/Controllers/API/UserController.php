<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Eloquent\User;
use Config;
use Hash;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function toggleActive(Request $request)
    {

        try {
            $user = User::findOrFail($request->id);
            if ($user->status === Config::get('constants.user_status.banned')) {
                $newState = Config::get('constants.user_status.active');
            } else {
                $newState = Config::get('constants.user_status.banned');
            }
            $user->status = $newState;
            $newState = Config::get('constants.user_status.banned');
            $user->update();
            return response()->json(['status' => true, 'msg' => 'Cập nhật thành công', 'data' => $user]);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'msg' => 'Action failed! ' . $e->getMessage(), 'data' => $user]);
        }
    }
    public function getAllSales()
    {
        $users = User::with(['roles' => function ($q) {
            $q->where('name', 'sale');
        }])->get();
        return view('backend.salesman.index');
    }
    public function resetPassword(Request $request)
    {
        $user_id = $request->user_id;
        $password = $request->password;
        $user = User::find($user_id);
        if ($user) {
            $user->password = Hash::make($password);
        }
        try {
            $user->update();
            return response()->json(['status' => true, 'msg' => 'Password đã đuợc thay đổi!']);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'msg' => 'Đổi password thất bại!  Lỗi: ' . $e->getMessage()]);
        }
    }
    public function getAll()
    {
        $users = User::with('roles')->get();
        return $users;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post_user = $request->user;
        $user = new User();
        $user->name = $post_user['name'];
        $user->email = $post_user['email'];
        $user->phone = $post_user['phone'];
        $user->password = Hash::make($post_user['password']);
        try{
            $user->save();
            $user->roles()->attach($post_user['role_id']);
            return response()->json(['status' => true, 'msg' => 'Thêm mới người dùng thành công!','data'=>$user]);
        }catch(\Exception $e)
        {
            return response()->json(['status' => false, 'msg' => 'Không thể tạo mới người dùng! Lỗi: '.$e->getMessage(),'data'=>$user]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
