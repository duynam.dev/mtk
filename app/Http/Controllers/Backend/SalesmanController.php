<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Agency;
use App\Role;
use App\Http\Requests\StoreUser;
use Session;
use App\Models\Contracts\SalesmanRepositoryInterface;
use App\Models\Eloquent\Payment;
use DB;

class SalesmanController extends Controller
{
    /**
     * @var App\Models\Contracts\SalesmanRepositoryInterface
     */
    protected $salesmanRepository;
    public function __construct(SalesmanRepositoryInterface $salesmanRepository)
    {
        $this->salesmanRepository = $salesmanRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $salesmen = $this->salesmanRepository->getAll();
        return view('backend.salesman.index', compact('salesmen'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.salesman.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUser $request)
    {
        $user = $this->salesmanRepository->create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => Hash::make($request->password),
        ]);
        if($user)
        {
            Session::flash('message','Thêm mới nhân viên bán hàng thành công');
        }
        Session::flash('message','Error! Không thể tạo mới nhân viên');
        if($request->has('save-continue'))
        {
            return redirect()->route('be.salesman.edit',$user);
        }
            return redirect()->route('be.salesman.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $salesman = $this->salesmanRepository->findWithRelationship($id,['agencies']);
        $statisticMonthRevenue = DB::table('payments')
        ->where('user_id',$id)
        ->whereMonth('created_at',date('m'))
        ->select(DB::raw('SUM(amount) as total, DAY(created_at) as day, MONTH(created_at) as month'))
        ->groupBy(DB::raw('DAY(created_at) ASC, MONTH(created_at) ASC'))->get()->toArray();
        //dd(DB::getQueryLog());

    
        $dayLabels = array();
        $dayRevenues = array();
        foreach($statisticMonthRevenue as $statistic)
        {
            array_push($dayLabels, 'Day '.$statistic->day);
            array_push($dayRevenues, $statistic->total);
        }
        $daychartjs = app()->chartjs
        ->name('lineChartTest')
        ->type('line')
        ->size(['width' => 400, 'height' => 200])
        ->labels($dayLabels)
        ->datasets([
            [
                "label" => "Doanh số trong tháng ".date('m'),
                'backgroundColor' => "rgba(38, 185, 154, 0.31)",
                'borderColor' => "rgba(38, 185, 154, 0.7)",
                "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                "pointHoverBackgroundColor" => "#fff",
                "pointHoverBorderColor" => "rgba(220,220,220,1)",
                'data' => $dayRevenues,
            ],
        ])
        ->options([
            'height'=> 300,
            'responsive' => true,
            'title' =>[
                'display'=> true,
                'text'=> "Doanh số trong tháng ".date('m'),
            ],
            'tooltips' => [
                'mode'=> 'index',
                'intersect'=> false,
            ],
            'hover' => [
                'mode' => 'nearest',
                'intersect'=> true
            ],
            'scales'=> [
                'xAxes'=> [
                    [
                    'display'=> true,
                    'scaleLabel' => [
                        'display' => true,
                        'labelString'=> 'Ngày'
                    ]
                    ]
                ],
                'YAxes'=> [
                    [
                    'display'=> true,
                    'scaleLabel' => [
                        'display' => true,
                        'labelString'=> 'Doanh thu'
                    ]
                    ]
                ],
            ]
        ]);

        $statisticYearRevenue = DB::table('payments')
        ->whereYear('created_at',date('Y'))
        ->select(DB::raw('SUM(amount) as total, MONTH(created_at) as month, YEAR(created_at) as year'))
        ->groupBy(DB::raw('YEAR(created_at) ASC, MONTH(created_at) ASC'))->get()->toArray();
        //dd(DB::getQueryLog());

    
        $monthsLabels = array();
        $monthsRevenues = array();
        foreach($statisticYearRevenue as $statistic)
        {
            array_push($monthsLabels, $statistic->month);
            array_push($monthsRevenues, $statistic->total);
        }


        $yearchartjs = app()->chartjs
        ->name('yearRevenueChart')
        ->type('line')
        ->size(['width' => 400, 'height' => 200])
        ->labels($monthsLabels)
        ->datasets([
            [
                "label" => "Doanh số trong năm ".date('Y'),
                'backgroundColor' => "rgba(38, 185, 154, 0.31)",
                'borderColor' => "rgba(38, 185, 154, 0.7)",
                "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                "pointHoverBackgroundColor" => "#fff",
                "pointHoverBorderColor" => "rgba(220,220,220,1)",
                'data' => $monthsRevenues,
            ],
        ])
        ->options([
            'height'=> 300,
            'responsive' => true,
            'title' =>[
                'display'=> true,
                'text'=> "Doanh số trong năm ".date('Y'),
            ],
            'tooltips' => [
                'mode'=> 'index',
                'intersect'=> false,
            ],
            'hover' => [
                'mode' => 'nearest',
                'intersect'=> true
            ],
            'scales'=> [
                'xAxes'=> [
                    [
                    'display'=> true,
                    'scaleLabel' => [
                        'display' => true,
                        'labelString'=> 'Tháng'
                    ]
                    ]
                ],
                'YAxes'=> [
                    [
                    'display'=> true,
                    'scaleLabel' => [
                        'display' => true,
                        'labelString'=> 'Doanh thu'
                    ]
                    ]
                ],
            ]
        ]);

        if(!$salesman)
        {
            abort(404);
        }
        //var_dump($salesman);
        return view('backend.salesman.edit',compact('salesman','daychartjs','yearchartjs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function addAgencies(Request $request, $salesman_id){
        $salesman = $this->salesmanRepository->find($salesman_id);
        return view('backend.salesman.addAgencies')->withSalesman($salesman);
    }
}
