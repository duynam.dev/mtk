<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contracts\AgencyRepositoryInterface;
use App\Models\Eloquent\Ward;
use App\Models\Eloquent\District;
use App\Models\Eloquent\Province;
use App\Models\Eloquent\Customer;
use App\Models\Eloquent\Agency;
use App\Models\Eloquent\User;
use Hash;
use Session;
use DB;

class AgencyController extends Controller
{
    private $agencyRepository;

    public function __construct(AgencyRepositoryInterface $agencyRepository)
    {
        $this->agencyRepository = $agencyRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$agencies = $this->agencyRepository->getAll();
      
        // $agencies = Agency::all();
        return view('backend.agency.index', compact('agencies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $salesmen = User::salesman()->orderBy('name','asc')->get();
        $provinces = Province::all();
        return view('backend.agency.create')->withProvinces($provinces)->withSalesmen($salesmen)    ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer = Customer::create($request->only(['fullname','email','phone_number','gender']));
        $agency = new Agency();
        $agency->name = $request->name;
        $agency->address = $request->address;
        $agency->province_id = $request->province_id;
        $agency->district_id = $request->district_id;
        $agency->ward_id = $request->ward_id;
        if($request->has('sale_id') && $request->sale_id != 0){
            $agency->sale_id = $request->sale_id;
        }
        
        if($agency->save()){
            Session::flash('message','Tạo mới đại lý thành công');
            Session::flash('alert-class','alert-success');
            $customer->agency_id = $agency->id;
            $customer->is_owner = 1;
            $customer->update();
        }
       

            Session::flash('message','Tạo mới đại lý không thành công');
            Session::flash('alert-class','alert-danger');

        return redirect()->route('be.agency.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $agency = Agency::with('customers')->where('id',$id)->first();
        return view('backend.agency.edit',compact('agency'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $agency = Agency::with(['province','district','ward','customers','salesman'])->where('id',$id)->first();
        $owner = Customer::where('agency_id',$id)->where('is_owner',1)->first();
     //   DB::enableQueryLog();
        $provinces = Province::all();
        $statisticYearRevenue = DB::table('payments')
        ->whereYear('created_at',date('Y'))
        ->where('agency_id',$id)
        ->select(DB::raw('SUM(amount) as total, MONTH(created_at) as month, YEAR(created_at) as year'))
        ->groupBy(DB::raw('YEAR(created_at) ASC, MONTH(created_at) ASC'))->get()->toArray();
      //  dd(DB::getQueryLog());

    
        $monthsLabels = array();
        $monthsRevenues = array();
        foreach($statisticYearRevenue as $statistic)
        {
            array_push($monthsLabels, $statistic->month);
            array_push($monthsRevenues, $statistic->total);
        }


        $yearchartjs = app()->chartjs
        ->name('yearRevenueChart')
        ->type('line')
        ->size(['width' => 400, 'height' => 200])
        ->labels($monthsLabels)
        ->datasets([
            [
                "label" => "Doanh số trong năm ".date('Y'),
                'backgroundColor' => "rgba(38, 185, 154, 0.31)",
                'borderColor' => "rgba(38, 185, 154, 0.7)",
                "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                "pointHoverBackgroundColor" => "#fff",
                "pointHoverBorderColor" => "rgba(220,220,220,1)",
                'data' => $monthsRevenues,
            ],
        ])
        ->options([
            'height'=> 300,
        ]);

        return view('backend.agency.edit',compact('agency','yearchartjs','owner','provinces'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $agency = $this->agencyRepository->find($id);
        $agency->name = $request->name;
        $agency->address = $request->address;
        if($agency->update())
        {
            Session::flash('message','Cập nhật thông tin đại lý thành công');
            Session::flash('alert-class','alert-success');
        }
            Session::flash('message','Error! Không cập nhật được thông tin đại lý!');
            Session::flash('alert-class','alert-danger');

        return redirect()->route('be.agency.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function import()
    {
        $agencies = file_get_contents('agencies.json');
        $agencies = json_decode($agencies);
        //  print_r($agencies[0]->A);
        //  die;
         $i = 1;
       //  return true;
        foreach($agencies as $raw_agency)
        {
          //  var_dump($agency[1]);
            // die;
            if($raw_agency !== null && $raw_agency->D !== null){
                $agency = new Agency();
                $agency->name = preg_replace('/\s\s+/', ' ',trim($raw_agency->B));
                $agency->address = $raw_agency->C;
                $agency->save();
              
                if($raw_agency->D !== null){
                    $owner = new Customer();
                    $owner->fullname = $agency->name;
                    $owner->phone_number = trim($raw_agency->D);
                    $owner->is_owner = 1;
                    $owner->agency_id = $agency->id;
                    $owner->save();
                }
                if($raw_agency->E !==null){
                    $sale = User::where('name',trim($raw_agency->E))->first();
                    if(empty($sale))
                    {
                        $phone_str = str_replace(' ','',$raw_agency->F);
                        $phone_1 = $phone_str;
                        $phone_2 = '';
                        if(strpos($phone_str,'-') !== false)
                        {
                            $phone_arr = explode('-',$phone_str);
                            $phone_1 = $phone_arr[0];
                            $phone_2 = $phone_arr[1];
                        }
                        $sale = User::create(['name' => $raw_agency->E,'phone' => $phone_1,'phone1' => $phone_2,'email' => 'sale_'.$i.'@mtk.com.vn', 'password' => Hash::make('12345678')]);
                        $sale->roles()->attach(1);
                        $i++;
                    }
                    $agency->sale_id = $sale->id;
                    $agency->update();   

                }
               
                
            }
          
                //die;
            // echo $agency[1]."<br>";
            // echo $agency[2]."<br>";
            // echo $agency[3]."<br>";
            // echo "<hr>";
        }


    }
}
