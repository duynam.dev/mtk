<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class DashboardController extends Controller
{
    public function index()
    {
        $statisticMonthRevenue = DB::table('payments')
        ->whereMonth('created_at',date('m'))
        ->whereYear('created_at', date('Y'))
        ->select(DB::raw('SUM(amount) as total, DAY(created_at) as day, MONTH(created_at) as month'))
        ->groupBy(DB::raw('DAY(created_at) ASC, MONTH(created_at) ASC'))->get()->toArray();
        //dd(DB::getQueryLog());
        $dayLabels = array();
        $dayRevenues = array();
        foreach($statisticMonthRevenue as $statistic)
        {
            array_push($dayLabels, 'Day '.$statistic->day);
            array_push($dayRevenues, $statistic->total);
        }
        $daychartjs = app()->chartjs
        ->name('lineChartTest')
        ->type('line')
        ->size(['width' => 400, 'height' => 200])
        ->labels($dayLabels)
        ->datasets([
            [
                "label" => "Doanh số trong tháng ".date('m'),
                'backgroundColor' => "rgba(38, 185, 154, 0.31)",
                'borderColor' => "rgba(38, 185, 154, 0.7)",
                "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                "pointHoverBackgroundColor" => "#fff",
                "pointHoverBorderColor" => "rgba(220,220,220,1)",
                'data' => $dayRevenues,
            ],
        ])
        ->options([
            'height'=> 300,
            'responsive' => true,
            'title' =>[
                'display'=> true,
                'text'=> "Doanh số trong tháng ".date('m'),
            ],
            'tooltips' => [
                'mode'=> 'index',
                'intersect'=> false,
            ],
            'hover' => [
                'mode' => 'nearest',
                'intersect'=> true
            ],
            'scales'=> [
                'xAxes'=> [
                    [
                    'display'=> true,
                    'scaleLabel' => [
                        'display' => true,
                        'labelString'=> 'Ngày'
                    ]
                    ]
                ],
                'YAxes'=> [
                    [
                    'display'=> true,
                    'scaleLabel' => [
                        'display' => true,
                        'labelString'=> 'Doanh thu'
                    ]
                    ]
                ],
            ]
        ]);

        $statisticYearRevenue = DB::table('payments')
        ->whereYear('created_at',date('Y'))
        ->select(DB::raw('SUM(amount) as total, MONTH(created_at) as month, YEAR(created_at) as year'))
        ->groupBy(DB::raw('YEAR(created_at) ASC, MONTH(created_at) ASC'))->get()->toArray();
        //dd(DB::getQueryLog());

    
        $monthsLabels = array();
        $monthsRevenues = array();
        foreach($statisticYearRevenue as $statistic)
        {
            array_push($monthsLabels, $statistic->month);
            array_push($monthsRevenues, $statistic->total);
        }


        $yearchartjs = app()->chartjs
        ->name('yearRevenueChart')
        ->type('line')
        ->size(['width' => 400, 'height' => 200])
        ->labels($monthsLabels)
        ->datasets([
            [
                "label" => "Doanh số trong năm ".date('Y'),
                'backgroundColor' => "rgba(38, 185, 154, 0.31)",
                'borderColor' => "rgba(38, 185, 154, 0.7)",
                "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                "pointHoverBackgroundColor" => "#fff",
                "pointHoverBorderColor" => "rgba(220,220,220,1)",
                'data' => $monthsRevenues,
            ],
        ])
        ->options([
            'height'=> 300,
            'responsive' => true,
            'title' =>[
                'display'=> true,
                'text'=> "Doanh số trong năm ".date('Y'),
            ],
            'tooltips' => [
                'mode'=> 'index',
                'intersect'=> false,
            ],
            'hover' => [
                'mode' => 'nearest',
                'intersect'=> true
            ],
            'scales'=> [
                'xAxes'=> [
                    [
                    'display'=> true,
                    'scaleLabel' => [
                        'display' => true,
                        'labelString'=> 'Tháng'
                    ]
                    ]
                ],
                'YAxes'=> [
                    [
                    'display'=> true,
                    'scaleLabel' => [
                        'display' => true,
                        'labelString'=> 'Doanh thu'
                    ]
                    ]
                ],
            ]
        ]);
    	return view('backend.dashboard.index', compact('yearchartjs','daychartjs'));
    }
}
