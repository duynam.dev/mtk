<?php

namespace App\Http\Controllers\Backend;

use App\Models\Eloquent\Payment;
use App\Models\Eloquent\Order;
use App\Models\Eloquent\Customer;
use App\Models\Eloquent\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Config;
use Event;
use App\Events\HelloPusherEvent;
use App\Events\PaymentVerified;
use Session;
use App\Models\Eloquent\Agency;
use App\Models\Eloquent\SMSLogging;
use App\Events\PaymentCreated;
use App\Utils\SMS;
use Auth;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     
        // $min = strtotime('2018-01-01 00:00:00');
        // $max = strtotime('2018-12-31 00:00:00');
        // $int = rand($min,$max);
        //echo date('Y-m-d H:i:s',$int);
    //     $min = strtotime('2018-01-01 00:00:00');
    //     $max = strtotime('2018-12-31 00:00:00');
    //    for($i = 1; $i<100; $i++){
           
         
    //        $int = rand($min,$max);
    //        $payment = Payment::create([
    //            'type' => 2,
    //            'amount' => rand(10000000, 20000000),
    //            'agency_id' => rand(2, 30),
    //            'customer_id' => 6,
    //            'status' => 1,
    //            'created_at' => date('Y-m-d H:i:s',$int),
    //            'updated_at' => date('Y-m-d H:i:s',$int),
    //            'user_id' => 1,
    //        ]);
    //    }
       $payments = Payment::with('customer.agency','order')->get();
       return view('backend.payments.index',compact('payments'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $orders = Order::all();
        if(Auth::user()->isAdmin()){
            $agencies = Agency::all();
        }else{
            $agencies = Agency::where('sale_id',Auth::user()->id)->get();
        }
        
        $payment_method = Config::get('constants.payment_method');
        // var_dump($payment_method);
        // die;
        return view('backend.payments.create',compact('orders','payment_method','agencies'));
    }
    public function confirm(Request $request){
        $id = $request->id;
        $reference_code = $request->reference_code;
        $confirmed_at = date('Y-m-d H:i:s');
        $payment = Payment::findOrFail($id);
        if($payment->confirmed_at !=null){
            return response()->json(['status' => false, 'msg' => 'Giao dịch đã được xác nhận trước đó', 'data' => null]);
        }
        $payment->reference_code = $reference_code;
        $payment->confirmed_at = $confirmed_at;
        $payment->status = Config::get('constants.payment_status.completed');
        if(!$payment->update()){
            return response()->json(['status' => false, 'msg' => 'Không cập nhật được giao dịch', 'data' => null]);
        }
        return response()->json(['status' => true, 'msg' => 'Giao dịch đã được cập nhật', 'data' => $payment]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $payment = new Payment();

        $payment->customer_id = $request->customer_id;
        $customer = Customer::findOrFail($request->customer_id);
        $payment->amount = str_replace(',','',$request->amount);
       // $payment->order_id = $request->order_id;
        $payment->agency_id = $request->agency_id;
        $payment->status = Config::get('constants.payment_status.pending');
        //$payment->paid_date = date('Y-m-d H:i:s');
        $payment->user_id = Auth::user()->id;
     //  $payment->user_receive_id = 5;
        if($request->has('user_receive_id'))
        {
            $payment->user_receive_id = $request->user_receive_id;
        }
        else
        {
            $payment->user_receive_id = Agency::find($request->agency_id)->sale_id;
        }
        $payment->save();
       
        Session::flash('message','Giao dịch tạo thành công');
        Session::flash('alert-class','alert-success');
        if($request->ajax())
        {
            return response()->json(['status' => true, 'data' => $payment]);
        }
        return redirect()->route('be.payments.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function show(Payment $payment)
    {
        $payment = Payment::with('agency','customer')->where('id',$payment->id)->first();
        event(new PaymentCreated($payment));

        return view('backend.payments.show', compact('payment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function edit(Payment $payment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Payment $payment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Payment $payment)
    {
        //
    }
    public function createPaymentToCompany(Request $request){
        
    }
    public function webHook(Request $request)
    {
        $content = strtolower(trim($request->content));
        $phone = $request->phone;
        $payment = Payment::where('verify_phone_number',$phone)->orderBy('id','desc')->first();
        $sms = new SMS();
        //echo strpos($phone, '84');
        if($content == "y"){
            if($payment){
                $time = date('m/d/Y h:i:s a', time());
                $payment->paid_date = date('Y-m-d H:i:s');
                $payment->update();
                $message = "KH: <strong>$payment->person_pay_name</strong> ($payment->verify_phone_number) đã xác nhận giao dịch số $payment->id - Thanh toán <strong>$payment->amount</strong> VND cho đơn hàng số <strong>$payment->order_id</strong>";
                event(new PaymentVerified($payment,$time,$message));
            }else{

                $result = $sms->sendSMS([$phone],'Khong tim thay GD nao dang ky bang sdt nay.', 2,  '0934594699');
            }

        }else{
                $result = $sms->sendSMS([$phone],'Cu phap tra loi khong chinh xac.', 2,  '0934594699');
        }

        return $phone;
        //return var_dump($request);
    }
    public function showVerifyForm($id)
    {
        $payment = Payment::with('agency','customer')->where('id',$id)->first();
        if($payment->status != Config::get('constants.payment_status.pending'))
        {
            Session::flash('message','Chỉ có thể tiến hành xác thực đối với các giao dịch ở trạng thái Pending');
            Session::flash('alert-class','alert-danger');
            return redirect()->route('be.payments.index');
        }
        return view('backend.payments.verify', compact('payment'));
    }
    public function pinVerify(Request $request)
    {
        $pin = $request->pin_code;
        $payment_id = $request->payment_id;
        $payment = Payment::with('customer')->where('id',$payment_id)->first();
        if($payment->status != Config::get('constants.payment_status.pending'))
        {
            Session::flash('message','Chỉ có thể tiến hành xác thực đối với các giao dịch ở trạng thái Pending');
            Session::flash('alert-class','alert-danger');
            return redirect()->route('be.payments.index');
        }
        $now = date("Y-m-d H:i:s");

        if((strtotime(date("Y-m-d H:i:s")) - strtotime($payment->created_at)) > 300){
            $payment->status = Config::get('constants.payment_status.out_date');
            $payment->update();
            Session::flash('message','Thời gian để xác nhận đã hết, vui lòng tạo giao dịch mới!');
            Session::flash('alert-class','alert-danger');
            return redirect()->route('be.payments.create');
        }
        $sms = new SMS();
        $result = $sms->verifyCode($payment->customer->phone_number,$pin);
        if($result['status'] == 'success' && $result['data']['verified'])
        {
            $payment->status = 2;
            $payment->verify_code = $result['data']['pin'];
            $payment->verified_at = date('Y-m-d H:i:s');
            $payment->update();
            Session::flash('message','Giao dịch xác nhận thành công!');
            Session::flash('alert-class','alert-success');
            return redirect()->route('be.payments.show',$payment);

        }
        if($result['status'] == 'success' && $result['data']['verified']==false && $result['data']['remainingAttempts'] > 0)
        {
            Session::flash('message','Mã pin vừa nhập không chính xác! Bạn còn '.$result['data']['remainingAttempts'].' lần nhập');
            Session::flash('alert-class','alert-danger');
            return redirect()->route('be.payments.verify.input',$payment);
        }
        if($result['data']['remainingAttempts'] == 0)
        {
            Session::flash('message','Số lần nhập mã pin đã hết, giao dịch vừa thực hiện sẽ bị hủy, vui lòng tạo giao dịch mới.');
            Session::flash('alert-class','alert-danger');
            $payment->status = Config::get('constants.payment_status.canceled');
            $payment->update();
            return redirect()->route('be.payments.create');
        }
            Session::flash('message','Không xác nhận được giao dịch! Lỗi: '.$result['data']['message']);
            Session::flash('message','alert-danger');
            return redirect()->route('be.payments.verify.input',$payment);
    }
}
