<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Eloquent\Province;
use App\Models\Eloquent\District;
use App\Models\Eloquent\Ward;

class AddressController extends Controller
{
    public function getDistrictsByProvinceId(Request $request)
    {
        $province_id = $request->province_id;
        $districts = District::where('province_id',$province_id)->get();
        return response()->json($districts);
    }
    public function getWardsByDistrictId(Request $request)
    {
        $wards = Ward::where('district_id',$request->district_id)->get();
        return response()->json($wards);
    }
    public function test()
    {
        $string = file_get_contents('provinces.json');
        $provinces = json_decode($string);  
        // var_dump($provinces);
        // die;
        foreach($provinces as $j_province)
        {
            $province = Province::where('name',trim($j_province->name))->first();
            if($province)
            {
               $province->post_code = $j_province->new_code;
               $province->update();
            }else{
                echo "Not Found:".$j_province->name;
            }
        }
    }
}
