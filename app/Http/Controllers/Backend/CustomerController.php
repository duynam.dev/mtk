<?php

namespace App\Http\Controllers\Backend;

use App\Models\Eloquent\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Eloquent\Agency;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::with('agency')->get();
        return view('backend.customers.index',compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $agencies = Agency::orderBy('name')->get();
        return view('backend.customers.create', compact('agencies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer = new Customer();
        $customer->fullname = $request->fullname;
        $customer->phone_number = $request->phone_number;
        $customer->address = $request->address;
        $customer->email = $request->email;
        if($request->agency_id != 0)
        {
            $customer->agency_id = $request->agency_id;
        }
        $customer->save();

        return redirect()->route('be.customers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        $agencies = Agency::orderBy('name')->get();
        return view('backend.customers.edit',compact('customer','agencies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        $customer->fullname = $request->fullname;
        $customer->phone_number = $request->phone_number;
        $customer->address = $request->address;
        $customer->email = $request->email;
        if($request->agency_id != 0)
        {
            $customer->agency_id = $request->agency_id;
        }

        $customer->update();

        return redirect()->route('be.customers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        //
    }
    public function ajaxGetCustomer(Request $request)
    {
        $id = $request->id;
        $customer = Customer::findorFail($id);

        return response()->json(['status' => true, 'customer' => $customer]);
    }
    public function ajaxGetCustomerByAgency(Request $request)
    {
        $agency_id = $request->agency_id;
        $customers = Customer::where('agency_id',$agency_id)->get();
        if(count($customers)>0)
        {
            return response()->json(['status' => true, 'customers' => $customers]);
        }
            return response()->json(['status' => false, 'message' => 'Đại lý này chưa có người đại diện.']);


    }
}
