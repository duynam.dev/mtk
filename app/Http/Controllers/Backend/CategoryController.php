<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;

class CategoryController extends Controller
{
    public function index()
    {
    	$categories = Category::all();
    	return view('backend.category.index', compact('categories'));
    }
    //load create category form
    public function createForm()
    {
    	$categories = Category::all();
    	return view('backend.category.create', compact('categories'));
    }
    public function updateForm($id)
    {
    	$categories = Category::all();
    	$category = Category::findOrFail($id);
    	return view('backend.category.edit', compact('categories', 'category'));
    }
    public function store(Request $request)
    {
    	$category = new Category();
    	$category->name = $request->name;
    	$category->parent_id = $request->parent_id;
    	$category->save();
    	return redirect()->route('be.category.index');
    }
    public function save(Request $request)
    {
    	$category = Category::findOrFail($request->id);
    	$category->name = $request->name;
    	$category->parent_id = $request->parent_id;
    	$category->update();
    	return redirect()->route('be.category.index');
    }
    public function delete()
    {

    }
    public function testWebHook($id){
        if($id != 0){
            $category = new Category();
            $category->name = 'Test testWebHook';
            $category->parent_id = 0;
            $category->save();

            return response()->json(['status' => 'success', 'cate' => $category]);
        }
    }
}
