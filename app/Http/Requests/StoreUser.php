<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|unique:users|max:255',
            'phone' => 'required|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ];
    }
    public function messages()
    {
        return [
            'password.required' => 'Xin vui lòng nhập mật khẩu',
            'password.min:6' => 'Mật khẩu phải chứa tối thiểu 6 ký tự',
            'password.confirmed' => 'Hai mật khẩu bạn vừa nhập chưa khớp với nhau',
            'email.email' => 'Email vừa nhập không hợp lệ',
            'email.unique' => 'Email vừa nhập đã tồn tại trên hệ thống',
            'email.required' => 'Vui lòng nhập địa chỉ email',
        ];
    }
}
