<?php

namespace App\Http\Requests\API;

class UserLoginRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'password' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'email' => 'Vui lòng nhập địa chỉ email hợp lệ',
            'password' => 'Bạn cần phải nhập mật khẩu'
        ];
    }
}
