<?php
namespace App\Services;

use App\Events\SMSEvent;
use App\Models\Eloquent\Payment;
use App\Models\Eloquent\SMSLogging;
use App\Utils\SMS;
use Config;

class SMSNotification
{
    private $sms;
    private $payment;
    private $is_update;
    public function __construct(Payment $payment, $is_update = false)
    {
        $this->is_update = $is_update;
        $this->payment = $payment;
        $this->sms = new SMS();
        $response = $this->sendSMS();
        //   $this->handleSMSResponse($response);
    }
    public function sendSMS()
    {
        $current_time = date('H:i');
        $current_date = date('d/m/Y');
        $paymentCreatedTime = date('H:i', strtotime($this->payment->created_at));
        $paymentCreatedDate = date('d/m/Y', strtotime($this->payment->created_at));
        if (!$this->is_update) {
            $sms_content = '[MTK] Vào lúc ' . $current_time . ' ngày ' . $current_date . ' giao dịch với số tiền ' . number_format($this->payment->amount) . ' tại đại lý ' . $this->payment->agency->name . ' đã được ghi nhận trên hệ thống.';
        } else {
            $sms_content = '[MTK] Cập nhật: Giao dịch vào lúc ' . $paymentCreatedTime . ' ngày ' . $paymentCreatedDate . ' tại đại lý ' . $this->payment->agency->name . 'đã được cập nhật lại trên hệ thống: Số tiền ' . number_format($this->payment->amount) . '.';
        }

        $response = $this->sms->sendSMS([$this->payment->customer->phone_number], $sms_content, 3, 'MTK');
        $this->handleSMSResponse($response, $sms_content);
        return $response;

    }
    public function handleSMSResponse($response, $sms_content)
    {
        $notification = array();
        if (!is_array($response)) {
            $notification['message'] = 'Oops!! Lỗi hệ thống, không thể gửi được tin nhắn';
            $notification['type'] = 'danger';
        } else {
            if ($response['status'] == 'success') {
                $notification['message'] = 'Tin nhắn gửi đi thành công!';
                $notification['type'] = 'success';
                SMSLogging::create([
                    'tranId' => $response['data']['tranId'],
                    'content' => $sms_content,
                    'payment_id' => $this->payment->id,
                    'customer_id' => $this->payment->customer->id,
                    'pin_code' => '9999',
                    'response' => 'Thành công',
                    'status' => 1,
                ]);
                $payment = Payment::find($this->payment->id);
                $payment->status = Config::get('constants.payment_status.pending');
                $payment->update();
            } else {
                SMSLogging::create([
                    'content' => $sms_content,
                    'payment_id' => $this->payment->id,
                    'customer_id' => $this->payment->customer->id,
                    'status' => 0,
                    'response' => 'Thất bại',
                    'error_code' => $response['code'],
                    'error' => $response['message'],
                ]);
                $notification['message'] = 'Không gửi được tin nhắn. Lỗi: ' . $response['message'];
                $notification['type'] = 'warning';

            }
        }
        broadcast(new SMSEvent($notification));
    }
    public function convert_multi_array($array)
    {
        $out = implode("&", array_map(function ($a) {return implode("~", $a);}, $array));
        return $out;
    }

}
