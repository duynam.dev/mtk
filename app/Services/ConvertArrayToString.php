<?php

namespace Services;

class ConvertArrayToString
{
    public function convert_multi_array($array)
    {
        $out = implode("&", array_map(function ($a) {return implode("~", $a);}, $array));
        return $out;
    }

}
