import swal from 'sweetalert2';

const alerts = {
    alert(msg) {
        swal(msg);
    },
    success(msg){
        swal({
            title: "Thành công!",
            text: msg,
            type: "success",
            button: "OK",
        });
    },
    error(msg){
        swal({
            title: "Thất bại!",
            text: msg,
            type: "error",
            confirmButtonText: "OK",
        });
    },
    warning(msg){
        swal({
            title: "Thất bại!",
            text: msg,
            type: "warning",
            confirmButtonText: "OK",
        });
    },
    ajaxRequest(title, param, okFunc){
        swal({
            title: title,
            input: 'text',
            inputAttributes: {
              autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            showLoaderOnConfirm: true,
            preConfirm: (login) => {
                okFunc(param, login);
                //return true;
                
            //   return fetch(`//api.github.com/users/${login}`)
            //     .then(response => {
            //       if (!response.ok) {
            //         throw new Error(response.statusText)
            //       }
            //       return response.json()
            //     })
            //     .catch(error => {
            //       swal.showValidationError(
            //         `Request failed: ${error}`
            //       )
            //     })
            },
            allowOutsideClick: () => !swal.isLoading()
          })
    }
}
export { alerts };