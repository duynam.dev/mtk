
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('bootstrap');
import Vue from 'vue'
window.moment = require('moment');
import BootstrapVue from 'bootstrap-vue';
import VueX from 'vuex'
import App from './App';
import {router} from './router'
import { event } from './utils'
import { http } from './services'
import axios from 'axios';
import VueAxios from 'vue-axios';
import 'axios-progress-bar/dist/nprogress.css';
Vue.prototype.$http = axios;

const token = localStorage.getItem('user-token')
if (token) {
  Vue.prototype.$http.defaults.headers.common['Authorization'] = token
}
Vue.config.devtools = true;
Vue.config.productionTip = false
Vue.use(VueAxios, axios);

Vue.use(BootstrapVue);
Vue.use(VueX);
import store from './vuex/store';




// Vue.axios.defaults.baseURL = 'http://mtk.test/api';
// Vue.config.devtools = true;
// Vue.router = router;
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
// Vue.component('livepaymentstable', require('./components/payments/LivePaymentsTable.vue'));
// Vue.component('paymentstable', require('./components/payments/PaymentsTable.vue'));
// Vue.use(require('@websanova/vue-auth'), {
//     auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
//     http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
//     router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js')
//   })
const app = new Vue({
    el: '#app',
    render: h => h(App),
    router,
    store,
    created () {
        event.init(),
        http.init()
    }
});