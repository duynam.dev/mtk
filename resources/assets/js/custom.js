
Echo.private('payment.created.'+window.Laravel.user)
.listen('PaymentCreated', (event) => {
    notify('Giao dịch số ' + event.payment.id + ' tại đại lý ' + event.payment.agency.name + ' đã được xác nhận!');
    audio.play();
});
Echo.private('payment.change')
.listen('PaymentCreated', (event) => { 
    notify('Giao dịch số ' + event.payment.id + ' tại đại lý ' + event.payment.agency.name + ' đã được xác nhận!');
    audio.play();
})


function notify(message) {
    $.notify({
        // options
        title: "<strong>Thông báo:</strong>",
        message: message,
        url: base_url+'payments'
    }, {
        // settings
        type: 'info',
        timer: 10000,

    });
}

