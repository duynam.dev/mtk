import Pusher from 'pusher-js'
import { userStore } from '../stores'
import { ls } from '.'
import Echo from 'laravel-echo'

export const socket = {
  pusher: null,
  channel: null,
  echo: null,
  async init () {
    return new Promise((resolve, reject) => {
   
      if (!process.env.MIX_PUSHER_APP_KEY) {
       
        return resolve()
      }

      this.pusher = new Pusher(process.env.MIX_PUSHER_APP_KEY, {
        authEndpoint: `${window.BASE_URL}api/broadcasting/auth`,
        auth: {
          headers: {
            Authorization: `Bearer ${ls.get('jwt-token')}`
          }
        },
        cluster: process.env.MIX_PUSHER_APP_CLUSTER,
        encrypted: true
      });

      this.echo = new Echo({
        broadcaster: 'pusher',
        authEndpoint: `${window.base_url}api/broadcasting/auth`,
        key: process.env.MIX_PUSHER_APP_KEY,
        cluster: process.env.MIX_PUSHER_APP_CLUSTER,
        encrypted: true,
        auth: {
            headers: {
                Authorization: `Bearer ${ls.get('jwt-token')}`
            }
        }
    });
  
      this.channel = this.pusher.subscribe('payment.change')
      this.channel.bind('pusher:subscription_succeeded', () => {
        return resolve()
      })
      this.channel.bind('pusher:subscription_error', () => {
        return resolve()
      })
  


    })
  },

  /**
   * Broadcast an event with Pusher.
   * @param  {string} eventName The event's name
   * @param  {Object} data      The event's data
   */
  broadcast (eventName, data = {}) {
    this.channel && this.channel.trigger(`client-${eventName}.${userStore.current.id}`, data)

    return this
  },

  /**
   * Listen to an event.
   * @param  {string}   eventName The event's name
   * @param  {Function} cb
   */
  listen (eventName, cb) {
    this.channel && this.channel.bind(`client-${eventName}.${userStore.current.id}`, data => cb(data))

    return this
  }
}
