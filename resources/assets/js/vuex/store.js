
import Vue from 'vue';
import VueX from 'vuex';
import axios from 'axios'
Vue.use(VueX);


export default new VueX.Store({
  state: {
    status: '',
    token: localStorage.getItem('token') || '',
    username : '',
    is_admin: ''
  },
  mutations: {
    auth_request(state){
      state.status = 'loading'
    },
    auth_success(state, token, user){
      state.status = 'success'
      state.token = token
      state.username = user.name,
      state.is_admin = user.is_admin
    },
    auth_error(state){
      state.status = 'error'
    },
    logout(state){
      state.status = ''
      state.token = ''
    },
  },
  actions: {
    login({commit}, credentials){
      return new Promise((resolve, reject) => {
        commit('auth_request')
        axios({url: 'auth/login', data: credentials, method: 'POST' })
        .then(resp => {
          let token = resp.data.token
          let user = resp.data.user
          console.log(user);
          localStorage.setItem('token', token)
          axios.defaults.headers.common['Authorization'] = token
          commit('auth_success', token, user)
          resolve(resp)
        })
        .catch(err => {
          commit('auth_error')
          localStorage.removeItem('token')
          reject(err)
        })
      })
  },
    logout({ commit }) {
      localStorage.removeItem("token");
      commit(LOGOUT);
    }
  },
	getters : {
	  isLoggedIn: state => !!state.token,
	  authStatus: state => state.status,
	}
});