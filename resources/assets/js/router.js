import Vue from 'vue'
import Router from 'vue-router'
import LivePaymentTable from './components/payments/LivePaymentsTable.vue'
import AgencyTable from './components/agency/index.vue'
import Login from './components/auth/Login.vue'
import AgencyEdit from './components/agency/edit'
import store from './vuex/store'
import Auth from './components/auth/Auth.vue'
import App from './App'
Vue.use(Router)
const router = new Router({
  linkActiveClass: 'active',
  routes: [
    {
      path:'/',
      name:'home',
      children:[
        {
          path: '/payment/live',
          name: 'payment.live',
          component: LivePaymentTable,
          meta:{
            requiresAuth: true
          },
        },
        {
          path: '/payment',
          name: 'payment.index',
          meta:{
            requiresAuth: true
          },
        },
        {
            path: '/agency',
            name: 'agency.index',
            meta:{
              requiresAuth: true
            },
            component: AgencyTable,
    
        },
        {
          path: '/agency/edit/:id',
          name: 'agency.edit',
          component: AgencyEdit,
          props: true,
          meta:{
            requiresAuth: true
          },
    
      },
      ]
    },
    {
      path: '/',
      name: 'Auth',
      component:Auth,
      children: [
        {
          path: '/login',
          name: 'login',
          component: Login,
    
        },
      ]
    }
  ]
});
router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.isLoggedIn) {
      next()
      return
    }
    next('/login') 
  } else {
    next() 
  }
})
export {router};