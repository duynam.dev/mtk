<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>@yield('title','Admin')</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="{{ asset('Stellar/node_modules/simple-line-icons/css/simple-line-icons.css') }}">
  <link rel="stylesheet" href="{{ asset('Stellar/node_modules/flag-icon-css/css/flag-icon.min.css') }}">}
  <!-- endinject -->
  <!-- plugin css for this page -->
  {{-- <link rel="stylesheet" href="{{ asset('Stellar/node_modules/chartist/dist/chartist.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('Stellar/node_modules/jvectormap/jquery-jvectormap.css') }}" /> --}}

  {{-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.material.min.css"> --}}
  {{-- <link rel="stylesheet" href="{{ asset('Stellar/node_modules/pwstabs/assets/jquery.pwstabs.min.css') }}"> --}}

  {{-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" /> --}}
  <!-- End plugin css for this page -->
  <!-- inject:css -->

  <link rel="stylesheet" href="{{ asset('Stellar/css/style.css') }}">
  <link rel="stylesheet" href="{{ asset('css/all.css') }}">
  <!-- endinject -->
  <link rel="shortcut icon" href="{{ asset('Stellar/images/favicon.png') }}" />
  <script>
    window.Laravel = {!! json_encode([
        'user' => auth()->check() ? auth()->user()->id : null,
    ]) !!};
</script>

</head>
<body>
  <div id="app">
  </div>
  <script>
    window.base_url = "{{ asset('') }}";
    window.audio = new Audio('{{ asset('sound/notification_up.mp3') }}');
  
    </script>
    <script>
            function Comma(Num) { //function to add commas to textboxes
            Num += '';
            Num = Num.replace(',', ''); Num = Num.replace(',', ''); Num = Num.replace(',', '');
            Num = Num.replace(',', ''); Num = Num.replace(',', ''); Num = Num.replace(',', '');
            x = Num.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1))
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            return x1 + x2;
        }</script>
    <script src="{{ asset('js/app.js') }}"></script>


  <!-- SOME SETTING AND GLOBAL JS FUNCTION-->


{{-- <script src="{{ asset('js/custom.js') }}"></script> --}}

  <!-- End custom js for this page-->
</body>

</html>
