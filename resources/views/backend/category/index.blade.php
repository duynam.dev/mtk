@extends('backend.layouts.index')
@section('title','Category')
@section('page-title','Category')
@section('parent-breadcrumb','Catalogue')
@section('child-breadcrumb','Category')
@section('content')
	<div class="col-lg-12">
		<div class="card">
			<div class="card-title">
			List
			<a href="{{ route('be.category.create') }}"><button class="btn btn-flat btn-success pull-right"><i class="fa fa-plus"></i>Add New</button></a>
			</div>
			<div class="card-body">
				<table class="table">
					<thead>
						<th>#</th>
						<th>Name</th>
						<th>Total Item(s)</th>
						<th>Action</th>
					</thead>
					<tbody>
						@foreach($categories as $category)
						<tr>
							<td>{{ $category->id }}</td>
							<td>{{ $category->name }}</td>
							<td>{{ $category->item_quantity }}</td>
							<td>
								<a href="{{ route('be.category.update',$category->id) }}"><button class="btn btn-flat"><i class="fa fa-pencil"></i> Edit</button></a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection