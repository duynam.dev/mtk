@extends('backend.layouts.index')
@section('title','Add New Category')
@section('page-title','Add Category')
@section('parent-breadcrumb','Category')
@section('child-breadcrumb','Create')
@section('content')
	<div class="col-lg-12">
		<form action="{{ route('be.category.store') }}" method="post">
			 @csrf
		<div class="card">
			<div class="card-title">
			Add New Category
			<button class="btn btn-flat btn-success pull-right" type="submit"><i class="fa fa-plus"></i>Save</button>
			</div>
			<div class="card-body">
				<div class="form-group">
					<label class="control-label">Name</label>
					<input type="text" name="name" class="form-control col-lg-4" required />
				</div>
				<div class="form-group">
					<label class="control-label">Parent Category</label>
					<select class="form-control col-lg-4" name="parent_id">]
						<option value="0">[None]</option>
						@foreach($categories as $category)
							<option value="{{ $category->id }}">{{ $category->name }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		</form>
	</div>
@endsection