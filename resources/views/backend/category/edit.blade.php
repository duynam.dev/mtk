@extends('backend.layouts.index')
@section('title','Update Category')
@section('page-title','Update Category')
@section('parent-breadcrumb','Category')
@section('child-breadcrumb','Update')
@section('content')
	<div class="col-lg-12">
		<form action="{{ route('be.category.save') }}" method="post">
			 @csrf
			 
		<input type="hidden" name="id" value="{{ $category->id }}">
		<div class="card">
			<div class="card-title">
			Update Category: <span class="text-info">{{ $category->name }}</span>
			<button class="btn btn-flat btn-success pull-right" type="submit"><i class="fa fa-plus"></i>Save</button>
			</div>
			<div class="card-body">
				<div class="form-group">
					<label class="control-label">Name</label>
					<input type="text" name="name" class="form-control col-lg-4" value="{{ $category->name }}" />
				</div>
				<div class="form-group">
					<label class="control-label">Parent Category</label>
					<select class="form-control col-lg-4" name="parent_id">]
						<option value="0">[None]</option>
						@foreach($categories as $a_category)
							<option value="{{ $a_category->id }}" @if($a_category->id == $category->parent_id) selected="true" @endif>{{ $a_category->name }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		</form>
	</div>
@endsection