<div class="modal fade" id="makePaymentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Thu tiền</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form method="post" action="{{ route('be.payments.store') }}">
                <input type="hidden" id="agencyId" value="">
                <input type="hidden" id="customerId" value="">
                <h5 class="col-form-label text-info">Thông tin đại lý: </h5>
                <div class="form-group row">
                  <label for="agency-name" class="col-form-label col-md-3">Tên đại lý:</label>
                  <span id="agency-name" class="font-weight-bold col-md-9"></span>
                </div>
                <div class="form-group row">
                    <label for="address" class="col-form-label col-md-3">Địa chỉ:</label>
                    <span id="address" class="font-weight-bold col-md-9"></span>
                </div>
                <h5 for="address" class="col-form-label text-info">Người thanh toán: </h5>
                <div class="form-group row">
                        <label for="customer_name" class="col-form-label col-md-3">Họ và tên:</label>
                        <span id="customer_name" class="font-weight-bold col-md-9"></span>
                </div>
                <div class="form-group row">
                    <label for="phone_number" class="col-form-label col-md-3">SĐT:</label>
                    <span id="phone_number" class="font-weight-bold col-md-9"></span>
                </div>
                <div class="form-group" id="changeCustomerContainer" style="visibility: hidden;">
                    <div class="col-md-6"> <select class="form-control" id="selectCustomer"></select></div>
                    <div class="col-md-6"><button type="button" id="confirmCustomerSelection" class="btn btn-success">Xác nhận</button></div>
                   
                </div>
                <button type="button" class="btn btn-info" id="changeCustomerBtn">Thay đổi</button>
                <hr>    
                <h5 for="address" class="col-form-label text-info">Người nhận:</h5>

                 <div class="form-group row">
                        <label for="salesman_name" class="col-form-label col-md-3">Họ và tên:</label>
                        <span id="salesman_name" class="font-weight-bold col-md-9"></span>
                </div>
                <div class="form-group row">
                        <label for="role" class="col-form-label col-md-3">Chức vụ:</label>
                        <span id="role" class="font-weight-bold col-md-9">Nhân viên kinh doanh</span>
                </div>
                <div class="form-group">
                  <label for="message-text" class="col-form-label">Số tiền:</label>
                  <div class="input-group">
                        
                        <input type="text" id="amount" class="form-control" aria-label="Amount" min="1000">
                        <div class="input-group-append">
                          <span class="input-group-text">VNĐ</span>
                        </div>
                      </div>
                </div>
                <div class="form-group">
                        <label for="message-text" class="col-form-label">Số tiền:</label>
                        <select name="payment_method" id="payment_method" class="form-control">
                            <option value="{!! Config::get('constants.payment_method.cash') !!}">Tiền mặt</option>
                            <option value="{!! Config::get('constants.payment_method.bank') !!}">Chuyển khoản</option>
                        </select>
                </div>

              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Hủy</button>
              <button type="button" class="btn btn-success" id="confirm-payment">Xác nhận</button>
            </div>
          </div>
        </div>
      </div>