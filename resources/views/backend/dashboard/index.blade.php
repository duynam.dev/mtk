@extends('backend.layouts.index')
@section('title','Dashboard')
@section('parent-breadcrumb','Trang chủ')
@section('child-breadcrumb','Dashboard')
@section('content')
<div class="row">
	<div class="col-lg-12">
						<!-- LINE CHART -->
						<div class="card card-info">
							
						  <div class="card-body">
									<h3 class="card-title">Biểu đồ doanh số tháng {!! date('m')  !!}</h3>
							<div class="chart">
							  {{-- <canvas id="lineChart" width="400" height="300"></canvas> --}}
							  {!! $daychartjs->render() !!}
							</div>
						  </div>
						  <!-- /.box-body -->
						</div>
						<!-- /.box -->
	</div>
</div>
	<div class="row">
			<div class="col-lg-12">
					<!-- LINE CHART -->
					<div class="card">
					
					  <div class="card-body">
								<h3 class="card-title">Biểu đồ doanh số  năm {!! date('Y')  !!}</h3>
						<div class="chart">
						  {{-- <canvas id="lineChart" width="400" height="300"></canvas> --}}
						  {!! $yearchartjs->render() !!}
						</div>
					  </div>
					  <!-- /.box-body -->
					</div>
					<!-- /.box -->
</div>
	</div>
@endsection