@extends('backend.layouts.index')
@section('title','Tạo mới đại lý')
@section('page-title','Tạo mới đại lý')
@section('parent-breadcrumb','Đại lý')
@section('child-breadcrumb','Tạo mới')
@section('content')
<form action="{{ route('be.agency.store') }}" method="post">
		@csrf
		<div class="row page-header">
				<div class="content-header col-md-6">
						<h2 class="page-title">
							Tạo mới đại lý
						</h2>
				</div>
			<div class="content-header col-md-6">
					<button class="btn btn-flat btn-success pull-right" type="submit"><i class="fa fa-save icon-sm"></i>Save</button>
			</div>
				
		</div>
		
		<div class="row">
		<div class="col-lg-6 grid-margin stretch-card">
			<div class="card">
				<div class="card-body">
						<h4 class="card-title">Thông tin đại lý</h4>
						
					<div class="form-group">
						<label class="control-label">Tên đại lý</label>
						<input type="text" name="name" class="form-control" required />
					</div>
					<div class="form-group">
						<label class="control-label">Tỉnh/Thành</label>
						<select class="select-2 form-control" name="province_id" id="province_sel">
							<option value="0">--Chọn một tỉnh/thành</option>
							@foreach($provinces as $province)
							<option value="{{ $province->id }}">{{ $province->name }}</option>
							
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label class="control-label">Quận/huyện</label>
						<select class="select-2 form-control" name="district_id" id="district_sel">
						</select>
					</div>
					<div class="form-group">
						<label class="control-label">Xã/Phường</label>
						<select class="select-2 form-control" name="ward_id" id="ward_sel">
						</select>
					</div>
					<div class="form-group">
						<label class="control-label">Địa chỉ</label>
						<input type="text" name="address" required class="form-control" placeholder="VD: 128 Nguyễn Thái Học"/>
					</div>
					<div class="form-group">
							<label class="control-label">Nhân viên bán hàng quản lý:</label>
							<select class="select-2 form-control" name="sale_id">
								<option value="0">-- Bổ sung sau --</option>
								@foreach($salesmen as $salesman)
								<option value="{{ $salesman->id }}">{{ $salesman->name }}</option>
								@endforeach
							</select>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-6 grid-margin stretch-card">
				<div class="card">
					<div class="card-body">
							<h4 class="card-title">Thông tin chủ cửa hàng </h4>
							
						<div class="form-group">
							<label class="control-label">Họ và tên</label>
							<input type="text" name="fullname" class="form-control" required />
						</div>
						<div class="form-group">
								<label class="control-label">Giới tính</label>
								<select name="gender" class="form-control">
									<option value="1">Nam</option>
									<option value="2">Nữ</option>
								</select>
							</div>
						<div class="form-group">
							<label class="control-label">SĐT</label>
							<input type="text" name="phone_number"" required class="form-control"/>
						</div>
						<div class="form-group">
							<label class="control-label">Email</label>
							<input type="email" name="email" required class="form-control" placeholder="(Nếu có)"/>
						</div>
					</div>
				</div>	
		</div>
	
		</form>
	</div>
@endsection
@section('scripts')
	<script>
		$(document).ready(function(){
			$("#province_sel").change(function(){
				var province_id = $(this).val();
				$.ajax({
					url: APP.API.district.getAllByProvinceId,
					method:'post',
					data:{
						province_id: province_id
					},
					success: function(data)
					{
						APP.appendOptionToSelect(data,"district_sel");
					}
				});
			});
			$("#district_sel").change(function(){
				var district_id = $(this).val();
				$.ajax({
					url: APP.API.ward.getAllByDistrictId,
					method:'post',
					data:{
						district_id: district_id
					},
					success: function(data)
					{
						APP.appendOptionToSelect(data,"ward_sel");
					}
				});
			})
		});
	</script>	
@endsection