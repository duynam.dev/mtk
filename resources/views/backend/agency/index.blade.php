@extends('backend.layouts.index')
@section('title','Danh sách đại lý')
@section('page-title','Đại lý')
@section('parent-breadcrumb','Catalogue')
@section('child-breadcrumb','Đại lý')
@section('content')
	<div class="col-lg-12">
		<div class="card">	    
			<div class="card-body">
				<h3 class="card-title">Danh sách  </h3>
				<table class="table table-hover" id="agenciesTable" width="100%">
					<thead>
						<th>#</th>
						<th>Tên đại lý</th>
						<th>Địa chỉ</th>

						<th>Tỉnh/Thành phố</th>
						<th>Khách hàng</th>
						<th>Doanh thu</th>
						<th>Sale</th>
						<th>Action</th>
						
					</thead>
					<tbody>
						
					</tbody>
				</table>
			</div>
		</div>
    </div>
@endsection
@section('modals')
    @include('backend.modals.addPaymentModal')
@endsection
@section('scripts')
<script src="{{ asset('Stellar/node_modules/inputmask/dist/jquery.inputmask.bundle.js') }}"></script>
<script src="{{ asset('Stellar/node_modules/inputmask/dist/jquery.inputmask.bundle.js') }}"></script>
<script src="{{ asset('Stellar/node_modules/inputmask/dist/min/inputmask/inputmask.numeric.extensions.min.js') }}"></script>
<script>
        //INPUT MASK
        $("#amount").inputmask({ alias : "currency", prefix:"", digits:0});

        //DATATABLES

	    var table = $("#agenciesTable").DataTable({
            ajax: "{{ route('api.agency.getAll') }}",
            "processing": true,
            "deferRender": true,
            columns:[
         
            { "data" : "id" },
            { "data" : "name" },
            { "data" : "address" },
            // { "data" : "ward.name" },
            // { "data" : "district.name" },
            { "data" : "province.name" },
			{ 
			  "data" : "customers",
			  "defaultContent": "-",
              render: function(data, row, index){
                  if(data.length > 0){
					  var html = "";
					  $.each(data, function(i, val){
						  html+= '<p>'+val.fullname+'</p>'
					  });
					  return html;
				  }
              }
			},
			{ "data" : "revenue",
			  render: function(data, row, index)
			  {
				  return Comma(data);
			  }
			},
			{
				"data" : "salesman.name",
				"defaultContent": "-"
			},
			{ "data" : "id",
              render: function(data, row, index){
                var buttons = '<a href="'+APP_URL+'agency/'+data+'/edit'+'" data-id="'+data+'" title="Sửa"><button class="btn btn-flat btn-xs btn-primary"><i class="fa fa-edit icon-xs"></i> Sửa</button></a>';
                buttons+= '<button class="make_payment btn btn-flat btn-xs  btn-info" data-id="'+data+'" data-toggle="modal" data-target="#makePaymentModal" title="Thu tiền"><i class="fa fa-money"></i> Thu tiền</button>';
                buttons+= '<button class="del_agency btn btn-flat btn-xs  btn-danger" data-id="'+data+'" title="Xóa"><i class="fa fa-trash"></i> Xóa </button>';
                return buttons;
              }
            }
            ],
        });
        
        //HANDLE EVENTS
		$("#agenciesTable").on('click','.del_agency', function(){
            var agency_id = $(this).attr('data-id');
            swal({
                title: "Xóa đại lý",
                text: "Bạn muốn xóa đại lý này khỏi hệ thống?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                })
                .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: '{{ route("api.agency.delete") }}',
                        method: 'post',
                        data:{
                            id: agency_id,
                        },
                        success: function(data){
							console.log(data);
                            if(data){
                                swal(" Xóa thành công", {
                    icon: "success",
                    });
                    table.ajax.reload();
                            }
                           
                        }
                    });
                    
                }
            });
        });
        $("#makePaymentModal").on('show.bs.modal', function(event){
            var button = $(event.relatedTarget);
            var agency_id = button.data('id');
           // console.log(APP);   
            APP.getAgency(agency_id,function(agency){
               // console.log(agency);
                $("#agency-name").html(agency.name);
                $("#agencyId").val(agency.id);
                $("#customerId").val(agency.owner.id);
                $("#address").html(agency.address + " - " +agency.ward.name+ " - " +agency.district.name+" - " +agency.province.name);
                $("#customer_name").html(agency.owner.fullname);
                $("#phone_number").html(agency.owner.phone_number);
                $("#salesman_name").html(agency.salesman.name);
                var sel = $("#selectCustomer");
                sel.find('option').remove();
                //console.log(select_id);
                sel.append('<option value="0" data-phone-number="">--Chọn khách hàng--</option>');
                $.each(agency.customers, function(index, value){
                //    console.log(value);
                    sel.append('<option data-phone-number="'+value.phone_number+'" value="'+value.id+'">'+value.fullname+'</option>');
                });
                // if(agency.customers.length == 1){
                //     $("#changeCustomerBtn").prop('disabled',true);
                // }
            //    $("#agency-name").html(agency.name));
            });

        });
        $("#changeCustomerBtn").click(function(){
           
            $("#changeCustomerContainer").css('visibility','visible');
        });
        $("#selectCustomer").change(function(){
            if($(this).val() != 0)
            {
                $("#customer_name").html($(this).find(':selected').text());
                $("#phone_number").html($(this).find(':selected').data('phone-number'));
                $("#customerId").val($(this).val());
            }
          
           // console.log($(this).find(':selected').data('phone-number'));
        });
       $("#confirmCustomerSelection").click(function(){
        $("#changeCustomerContainer").css('visibility','hidden');
       });
       $("#confirm-payment").click(function(){
           var amount = parseInt($("#amount").val().replace(/,/g, ''));
           if(amount>0){
               $(this).removeClass().addClass('btn btn-warning').html('<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Loading...');
               axios.post('/payments',
            {
                customer_id: $("#customerId").val(),
                     agency_id: $("#agencyId").val(),
                     amount: $("#amount").val(),
                     type: $("#payment_method").val(),
            })
            .then(function (response){
                if (response.status) {
                         $("#makePaymentModal").modal('hide');
                         swal({
                                 title: "Thành công!",
                                 text: "Thông tin giao dịch đã được lưu!",
                                 icon: "success",
                                 buttons: {
                                     payments: "Lịch sử giao dịch",
                                     continue: true,
                                 },
                             })
                             .then((value) => {
                                 switch (value) {

                                     case "payments":
                                     window.location.replace("{{ route('be.payments.index') }}");
                                         break;
                                     default:
                                         
                                 }
                             });
                     }
            });
            //     $.ajax({
            //      url: "{{ route('be.payments.store') }}",
            //      method: 'post',
            //      data:{
            //          customer_id: $("#customerId").val(),
            //          agency_id: $("#agencyId").val(),
            //          amount: $("#amount").val(),
            //          type: $("#payment_method").val(),
            //      },
            //      success: function(data){
            //          if (data.status) {
            //              $("#makePaymentModal").modal('hide');
            //              swal({
            //                      title: "Thành công!",
            //                      text: "Thông tin giao dịch đã được lưu!",
            //                      icon: "success",
            //                      buttons: {
            //                          payments: "Lịch sử giao dịch",
            //                          continue: true,
            //                      },
            //                  })
            //                  .then((value) => {
            //                      switch (value) {

            //                          case "payments":
            //                          window.location.replace("{{ route('be.payments.index') }}");
            //                              break;
            //                          default:
                                         
            //                      }
            //                  });
            //          }
            //      }
            //  })
            //alert($("#amount").val());

           }  
       });
</script>
@endsection