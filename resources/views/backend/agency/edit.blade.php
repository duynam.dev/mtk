@extends('backend.layouts.index') @section('title','Cập nhật thông tin đại lý') @section('page-title','Cập nhật thông tin
đại lý') @section('parent-breadcrumb','Đại lý') @section('child-breadcrumb','Cập nhật') @section('content')
<div class="vertical-tab">
	<ul class="nav nav-tabs tab-basic" role="tablist">
		<li class="nav-item active">
			<a href="#statistic" class="nav-link active show" data-toggle="tab" aria-expanded="false" role="tab">Thống kê</a>
		</li>
		<li class="nav-item">
			<a href="#info" class="nav-link" data-toggle="tab" aria-expanded="true">Thông tin</a>
		</li>
	</ul>
	<div class="tab-content">
		<div class="tab tab-pane active" id="statistic">
			<div class="row">
				<div class="col-md-5 grid-margin align-items-stretch">
					<div class="row flex-row">
						<div class="col-12 grid-margin">
							<div class="card">
								<div class="card-body">
									<h4 class="card-title mb-0">
										Thống kê 
									</h4>
									<div class="d-flex justify-content-between align-items-center">
										<div class="d-inline-block pt-3">
											<div class="d-flex">
												<h2 class="mb-0">$10,200</h2>
												<div class="d-flex align-items-center ml-2">
													<i class="mdi mdi-clock text-muted"></i>
													<small class=" ml-1 mb-0">Updated: 9:10am</small>
												</div>
											</div>
											<small class="text-gray">Raised from 89 orders.</small>
										</div>
										<div class="d-inline-block">
											<div class="bg-success px-4 py-2 rounded">
												<i class="mdi mdi-buffer text-white icon-lg"></i>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row flex-row">
						<div class="col-12 grid-margin">

							<div class="card">
								<div class="card-body">
									<div class="d-flex flex-row">
										@if(file_exists(asset('Stellar//images/faces/face22.jpg')))
										<img src="https://ui-avatars.com/api/?background=0D8ABC&color=fff" class="img-lg rounded">
										@else
										<img src="https://ui-avatars.com/api/?background=0D8ABC&color=fff&name={{ $agency->salesman->name }}&size=256" class="img-lg rounded">
										@endif
										<div class="ml-3">
											<h6>{{ $agency->salesman->name }}</h6>
											<p class="text-muted">{{ $agency->salesman->email }}</p>
											<p class="mt-2 text-success font-weight-bold">
												@foreach($agency->salesman->roles as $role)
												 @if($role->name == 'sale')
												 	{{ $role->description }}
												 @endif
												@endforeach
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="col-md-7 stretch-card">
					<!-- LINE CHART -->
					<div class="card">
						<div class="card-body">
							<h3 class="card-title">Biểu đồ doanh số năm {!! date('Y') !!}</h3>
							<div class="chart">
								{{--
								<canvas id="lineChart" width="400" height="300"></canvas> --}} {!! $yearchartjs->render() !!}
							</div>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
			</div>

		</div>
		<div class="tab tab-pane" id="info">

			<div class="card">


				<form action="{{ route('be.agency.update',$agency) }}" method="post">

					<div class="card-body">
						<h3 class="card-title">Cập nhật thông tin
							<button class="btn btn-flat btn-success pull-right" type="submit">
								<i class="fa fa-plus"></i>Save</button>
						</h3>

						@csrf @method('put')
						<div class="row">
							<div class="col-lg-6 grid-margin stretch-card">
								<div class="card">
									<div class="card-body">
										<h4 class="card-title">Thông tin đại lý</h4>

										<div class="form-group">
											<label class="control-label">Tên đại lý</label>
											<input type="text" name="name" value="{{ $agency->name }}" class="form-control" required />
										</div>
										<div class="form-group">
											<label class="control-label">Tỉnh/Thành</label>
											<select class="select-2 form-control" name="province_id" id="province_sel">
												<option value="0">--Chọn một tỉnh/thành</option>
												@foreach($provinces as $province)
												<option value="{{ $province->id }}" @if($province->id == $agency->province_id) selected @endif>{{ $province->name }}</option>

												@endforeach
											</select>
										</div>
										<div class="form-group">
											<label class="control-label">Quận/huyện</label>
											<select class="select-2 form-control" name="district_id" id="district_sel">
												<option value="{{ $agency->district_id }}" selected>{{ $agency->district->name }}</option>
											</select>
										</div>
										<div class="form-group">
											<label class="control-label">Xã/Phường</label>
											<select class="select-2 form-control" name="ward_id" id="ward_sel">
												<option value="{{ $agency->ward_id }}" selected>{{ $agency->ward->name }}</option>
											</select>
										</div>
										<div class="form-group">
											<label class="control-label">Địa chỉ</label>
											<input type="text" name="address" required class="form-control" value="{{ $agency->address }}" placeholder="VD: 128 Nguyễn Thái Học"
											/>
										</div>
										<div class="form-group">
											<label class="control-label">Nhân viên bán hàng quản lý:</label>

										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-6 grid-margin stretch-card">
								<div class="card">
									<div class="card-body">
										<h4 class="card-title">Thông tin chủ cửa hàng </h4>
										<input type="hidden" name="owner_id" value="{{ $owner->id }}">
										<div class="form-group">
											<label class="control-label">Họ và tên</label>
											<input type="text" name="fullname" value="{{$owner->fullname}}" class="form-control" required/>
										</div>
										<div class="form-group">
											<label class="control-label">Giới tính</label>
											<select name="gender" class="form-control">
												<option value="1">Nam</option>
												<option value="2">Nữ</option>
											</select>
										</div>
										<div class="form-group">
											<label class="control-label">SĐT</label>
											<input type="text" name="phone_number" value="{{ $owner->phone_number }}" class="form-control" required/>
										</div>
										<div class="form-group">
											<label class="control-label">Email</label>
											<input type="email" name="email" value="{{ $owner->email }}" required class="form-control" placeholder="(Nếu có)" />
										</div>
									</div>
								</div>
							</div>
						</div>
				</form>
				</div>
			</div>

		</div>
	</div>
</div>
@endsection