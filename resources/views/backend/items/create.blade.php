@extends('backend.layouts.index')
@section('title','Add New Item')
@section('page-title','Add Item')
@section('parent-breadcrumb','Item')
@section('child-breadcrumb','Create')
@section('content')
	<div class="col-lg-12">
		<form action="{{ route('be.items.store') }}" method="post">
			 @csrf
		<div class="card">
			<div class="card-title">
			Add New Item
			<button class="btn btn-flat btn-success pull-right" type="submit"><i class="fa fa-plus"></i>Save</button>
			</div>
			<div class="card-body">
				<div class="form-group">
					<label class="control-label">Code</label>
					<input type="text" name="code" class="form-control col-lg-4" required />
				</div>
				<div class="form-group">
					<label class="control-label">Name</label>
					<input type="text" name="name" class="form-control col-lg-4" required />
				</div>
				<div class="form-group">
					<label class="control-label">Price</label>
					<input type="number" name="price" class="form-control col-lg-4" required />
				</div>
				<div class="form-group">
					<label class="control-label">Quantity</label>
					<input type="number" name="quantity" class="form-control col-lg-4" required />
				</div>
				<div class="form-group">
					<label class="control-label">Category</label>
					<select class="form-control col-lg-4" name="category_id">]
						@foreach($categories as $category)
							<option value="{{ $category->id }}">{{ $category->name }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		</form>
	</div>
@endsection