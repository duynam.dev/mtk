@extends('backend.layouts.index')
@section('title','Edit Item')
@section('page-title','Edit Item')
@section('parent-breadcrumb','Item')
@section('child-breadcrumb','Edit')
@section('content')
	<div class="col-lg-12">
		<form action="{{ route('be.items.update',$item) }}" method="post">
			 @csrf
		<input name="_method" type="hidden" value="PUT">
		<div class="card">
			<div class="card-title">
			Edit Item
			<button class="btn btn-flat btn-success pull-right" type="submit"><i class="fa fa-plus"></i>Save</button>
			</div>
			<div class="card-body">
				<div class="form-group">
					<label class="control-label">Code</label>
					<input type="text" name="code" class="form-control col-lg-4" value="{{ $item->code }}" required />
				</div>
				<div class="form-group">
					<label class="control-label">Name</label>
					<input type="text" name="name" class="form-control col-lg-4" value="{{ $item->name }}" required />
				</div>
				<div class="form-group">
					<label class="control-label">Price</label>
					<input type="number" name="price" class="form-control col-lg-4" value="{{ $item->price }}" required />
				</div>
				<div class="form-group">
					<label class="control-label">Quantity</label>
					<input type="number" name="quantity" class="form-control col-lg-4" value="{{ $item->quantity }}" required />
				</div>
				<div class="form-group">
					<label class="control-label">Category</label>
					<select class="form-control col-lg-4" name="category_id">]
						@foreach($categories as $category)
							<option value="{{ $category->id }}" @if($category->id == $item->category_id) selected="true" @endif>{{ $category->name }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		</form>
	</div>
@endsection