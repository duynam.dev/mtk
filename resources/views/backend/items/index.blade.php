@extends('backend.layouts.index')
@section('title','Items')
@section('page-title','Item')
@section('parent-breadcrumb','Catalogue')
@section('child-breadcrumb','Item')
@section('content')
	<div class="col-lg-12">
		<div class="card">
			<div class="card-title">
			List
			<a href="{{ route('be.items.create') }}"><button class="btn btn-flat btn-success pull-right"><i class="fa fa-plus"></i>Add New</button></a>
			</div>
			<div class="card-body">
				<table class="table">
					<thead>
						<th>#</th>
						<th>Code</th>
						<th>Name</th>
						<th>Category</th>
						<th>Price</th>
						<th>Quantity</th>
						<th>Action</th>
					</thead>
					<tbody>
						@foreach($items as $item)
						<tr>
							<td>{{ $item->id }}</td>
							<td>{{ $item->code }}</td>
							<td>{{ $item->name }}</td>
							<td>{{ $item->category->name }}</td>
							<td>{{ $item->price }}</td>
							<td>{{ $item->quantity }}</td>
							<td>
								<a href="{{ route('be.items.edit',$item) }}"><button class="btn btn-flat"><i class="fa fa-pencil"></i> Edit</button></a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection