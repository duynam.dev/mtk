@extends('backend.layouts.index')
@section('title','Edit Customer')
@section('page-title','Edit Customer')
@section('parent-breadcrumb','Customer')
@section('child-breadcrumb','Edit')
@section('content')
	<div class="col-lg-12">
		<form action="{{ route('be.customers.update',$customer) }}" method="post">
			 @csrf
		<input name="_method" type="hidden" value="PUT">
		<div class="card">
		
		
			

			<div class="card-body">
					<h3 class="card-title">Chỉnh sửa thông tin KH <button class="btn btn-flat btn-success pull-right" type="submit"><i class="fa fa-save"></i> Save</button></h3>
				<div class="form-group">
					<label class="control-label">Tên Khách hàng</label>
					<input type="text" name="fullname" class="form-control col-lg-4" value="{{ $customer->fullname }}" required />
				</div>
				<div class="form-group">
					<label class="control-label">SĐT</label>
					<input type="text" name="phone_number" value="{{ $customer->phone_number }}" class="form-control col-lg-4"/>
				</div>
				<div class="form-group">
					<label class="control-label">Địa chỉ</label>
					<input type="text" name="address" value="{{ $customer->address }}" class="form-control col-lg-4"/>
				</div>
				<div class="form-group">
					<label class="control-label">Email</label>
					<input type="email" name="email" value="{{ $customer->email }}" class="form-control col-lg-4" />
				</div>
				<div class="form-group">
					<label class="control-label">Đại lý</label>
					<select class="form-control" name="agency_id">
						<option value="0">Không có</option>
						@foreach($agencies as $agency)
							<option value="{{ $agency->id }}" @if($customer->agency_id == $agency->id) selected @endif>{{ $agency->name }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		</form>
	</div>
@endsection