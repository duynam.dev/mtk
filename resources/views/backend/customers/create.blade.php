@extends('backend.layouts.index')
@section('title','Tạo mới Khách hàng')
@section('page-title','Tạo mới Khách hàng')
@section('parent-breadcrumb','Khách hàng')
@section('child-breadcrumb','Tạo mới')
@section('content')
	<div class="col-lg-12">
		<form action="{{ route('be.customers.store') }}" method="post">
			 @csrf
		<div class="card">
			<div class="card-body">
				<h3 class="card-title">Tạo khách hàng mới	<button class="btn btn-flat btn-success pull-right" type="submit"><i class="fa fa-plus"></i>Save</button></h3>
				<div class="form-group">
					<label class="control-label">Tên Khách hàng</label>
					<input type="text" name="fullname" class="form-control col-lg-4" required />
				</div>
				<div class="form-group">
					<label class="control-label">SĐT</label>
					<input type="text" name="phone_number" class="form-control col-lg-4"/>
				</div>
				<div class="form-group">
					<label class="control-label">Địa chỉ</label>
					<input type="text" name="address" class="form-control col-lg-4"/>
				</div>
				<div class="form-group">
					<label class="control-label">Email</label>
					<input type="email" name="email" class="form-control col-lg-4" />
				</div>
				<div class="form-group">
					<label class="control-label">Đại lý</label>
					<select class="form-control" name="agency_id">
						<option value="0">Không có</option>
						@foreach($agencies as $agency)
							<option value="{{ $agency->id }}">{{ $agency->name }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		</form>
	</div>
@endsection