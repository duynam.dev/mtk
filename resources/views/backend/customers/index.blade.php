@extends('backend.layouts.index')
@section('title','Khách hàng')
@section('page-title','Danh sách khách hàng')
@section('parent-breadcrumb','Catalogue')
@section('child-breadcrumb','Khách hàng')
@section('content')
	<div class="col-lg-12">
		<div class="card">
			<div class="card-body">
			<h3 class="card-title">List <a href="{{ route('be.customers.create') }}"><button class="btn btn-flat btn-success pull-right"><i class="fa fa-plus"></i>Tạo mới</button></a></h3>
			
				<table class="table">
					<thead>
						<th>#</th>
						<th>Tên KH</th>
						<th>Tên đại lý</th>
						<th>SĐT</th>
						<th>Địa chỉ</th>
						<th>Email</th>
						<th></th>
					</thead>
					<tbody>
						@foreach($customers as $customer)
						<tr>
							<td>{{ $customer->id }}</td>
							<td>{{ $customer->fullname }}</td>
							<td>@if($customer->agency){{ $customer->agency->name }}@else N/A @endif</td>
							<td>{{ $customer->phone_number }}</td>
							<td>{{ $customer->address }}</td>
							<td>{{ $customer->email }}</td>
							<td>
								<a href="{{ route('be.customers.edit',$customer) }}"><button class="btn btn-flat"><i class="fa fa-pencil"></i> Edit</button></a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection