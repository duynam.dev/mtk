@extends('backend.layouts.index')
@section('title','Lịch sử tin nhắn')
@section('page-title','Lịch sử tin nhắn')
@section('parent-breadcrumb','Catalogue')
@section('child-breadcrumb','Tin nhắn')
@section('content')
	<div class="col-lg-12">
		<div class="box box-success">
			<div class="box-header">
			<h3>Lịch sử tin nhắn</h3><br>
            <span><strong>Số dư tài khoản: </strong><span class="badge badge-info">{{ $sms_info['data']['balance'] }} đ</span></span>
			</div>
			<div class="box-body">
				<table class="table">
					<thead>
						<th>#</th>
						<th>Nội dung</th>

                        <th>Người nhận</th>
                        <th>SĐT</th>
						<th>Mã giao dịch</th>
						<th>Thời gian gửi</th>
					</thead>
					<tbody>
						@foreach($sms_logs as $sms)
						<tr>
							<td>{{ $sms->id }}</td>
                            <td>{{ $sms->content }}</td>
                            <td>{{ $sms->customer->fullname }}</td>
							<td>{{ $sms->customer->phone_number }}</td>

							<td>{{ $sms->payment_id }}</td>
							<td>{{ $sms->created_at }}</td>

						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection