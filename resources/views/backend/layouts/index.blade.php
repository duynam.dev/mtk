<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>@yield('title','Admin')</title>
  <!-- plugins:css -->
  {{-- <link rel="stylesheet" href="{{ asset('Stellar/node_modules/mdi/css/materialdesignicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('Stellar/node_modules/simple-line-icons/css/simple-line-icons.css') }}">
  <link rel="stylesheet" href="{{ asset('Stellar/node_modules/flag-icon-css/css/flag-icon.min.css') }}">
  <link rel="stylesheet" href="{{ asset('Stellar/node_modules/font-awesome/css/font-awesome.min.css') }}" /> --}}
  <!-- endinject -->
  <!-- plugin css for this page -->
  {{-- <link rel="stylesheet" href="{{ asset('Stellar/node_modules/chartist/dist/chartist.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('Stellar/node_modules/jvectormap/jquery-jvectormap.css') }}" /> --}}
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.0/material.min.css">
  {{-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.material.min.css"> --}}
  {{-- <link rel="stylesheet" href="{{ asset('Stellar/node_modules/pwstabs/assets/jquery.pwstabs.min.css') }}"> --}}

  {{-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" /> --}}
  <!-- End plugin css for this page -->
  <!-- inject:css -->
{{-- 
  <link rel="stylesheet" href="{{ asset('Stellar/css/style.css') }}"> --}}
  <link rel="stylesheet" href="{{ asset('css/all.css') }}">
  <!-- endinject -->
  <link rel="shortcut icon" href="{{ asset('Stellar/images/favicon.png') }}" />
  <script>
    window.Laravel = {!! json_encode([
        'user' => auth()->check() ? auth()->user()->id : null,
    ]) !!};
</script>
</head>
<body>
  <div class="container-scroller" id="app">
    <!-- partial:partials/_navbar.html -->
    @include('backend.layouts.partials.navbar')
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <div class="row row-offcanvas row-offcanvas-right">
        <!-- partial:partials/_sidebar.html -->
        @include('backend.layouts.partials.sidebar')
        <!-- partial -->
        <div class="content-wrapper">
            @yield('content')
          <!-- ROW ENDS -->
          <!-- ROW ENDS -->
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 <a href="http://www.bootstrapdash.com/" target="_blank">Bootstrapdash</a>. All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i></span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- row-offcanvas ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>



  <!-- SOME SETTING AND GLOBAL JS FUNCTION-->
  <script>
  window.BASE_URL = "{{ asset('') }}";
  window.notification_sound = new Audio('{{ asset('sound/notification_up.mp3') }}');
  var APP = {};
  APP.API = {
    district: {
      getAllByProvinceId: '{{ route("be.address.district.byProvince") }}',
    },
    ward: {
      getAllByDistrictId: '{{ route("be.address.ward.byDistrict") }}',
    },
    agency: {
      get: '{{ route("api.agency.get") }}'
    }
  }
  </script>


  {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> --}}
  {{-- <script src="{{ asset('Stellar/node_modules/sweetalert/dist/sweetalert.min.js')}}"></script> --}}
  {{-- <script src="{{ asset('plugin/bootstrap-notify/bootstrap-notify.min.js') }}"></script> --}}
  <script>
          function Comma(Num) { //function to add commas to textboxes
          Num += '';
          Num = Num.replace(',', ''); Num = Num.replace(',', ''); Num = Num.replace(',', '');
          Num = Num.replace(',', ''); Num = Num.replace(',', ''); Num = Num.replace(',', '');
          x = Num.split('.');
          x1 = x[0];
          x2 = x.length > 1 ? '.' + x[1] : '';
          var rgx = /(\d+)(\d{3})/;
          while (rgx.test(x1))
              x1 = x1.replace(rgx, '$1' + ',' + '$2');
          return x1 + x2;
      }</script>
  {{-- <script src="{{ asset('js/app.js') }}"></script> --}}
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>


        $(document).ready(function(){
          // $(".select-2").select2();
          
          $(".money-input").keyup(function(){
            this.value = Comma(this.value);
          });
        });
         
          
  
          // Enable pusher logging - don't include this in production
          // Pusher.logToConsole = true;
  
          // var pusher = new Pusher('6b952ee0b40139ef921f', {
          //   cluster: 'ap1',
          //   encrypted: true
          // });
  

        </script>
        <!-- Global Config Datatables -->
        <script>
          $.extend( $.fn.dataTable.defaults, {
            language:{
              processing:     "Đang xử lý",
          search:         "Tìm kiếm:",
          lengthMenu:    "Hiển thị _MENU_ mỗi trang",
          info:           "Đang hiển thị _START_ đến _END_ trong tổng sô _TOTAL_ dòng",
          infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
          infoFiltered:   "(Lọc từ _MAX_ tổng)",
          infoPostFix:    "",
          loadingRecords: "Đang tải dữ liêụ",
          zeroRecords:    "Không tìm thấy dữ liệu",
          emptyTable:     "Không có dữ liệu để hiển thị",
          paginate: {
              first:      "Trang đầu",
              previous:   "Trang trước",
              next:       "Trang sau",
              last:       "Trang cuối"
          },
          aria: {
              sortAscending:  ": activer pour trier la colonne par ordre croissant",
              sortDescending: ": activer pour trier la colonne par ordre décroissant"
          }
            }
          } );
</script>

{{-- <script src="{{ asset('js/custom.js') }}"></script> --}}
  @yield('scripts')
  <!-- End custom js for this page-->
</body>

</html>
