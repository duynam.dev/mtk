<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
      <li class="nav-item nav-category">
        <span class="nav-link">GENERAL</span>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="index.html">
          <span class="menu-title">Dashboard</span>
          <i class="icon-speedometer menu-icon"></i>
        </a>
      </li>
      <li class="nav-item nav-category">
        <span class="nav-link">KHÁCH HÀNG</span>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('be.customers.index') }}">
          <span class="menu-title">KHÁCH HÀNG</span>
          <i class="icon-layers menu-icon"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('be.agency.index') }}">
          <span class="menu-title">ĐẠI LÝ</span>
          <i class="icon-grid menu-icon"></i>
        </a>
      </li>
      <li class="nav-item nav-category">
        <span class="nav-link">GIAO DỊCH</span>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('be.payments.live') }}">
          <span class="menu-title">GIAO DỊCH HÔM NAY (LIVE)</span>
          <i class="icon-flag menu-icon"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('be.payments.index') }}">
          <span class="menu-title">LỊCH SỬ GIAO DỊCH</span>
          <i class="icon-flag menu-icon"></i>
        </a>
      </li>
      <li class="nav-item nav-category">
        <span class="nav-link">USERS</span>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('be.salesman.index') }}">
          <span class="menu-title">NHÂN VIÊN BÁN HÀNG</span>
          <i class="icon-bubbles menu-icon"></i>
        </a>
        <a class="nav-link" href="#auth">
                <span class="menu-title">Administrator</span>
                <i class="icon-bubbles menu-icon"></i>
        </a>
      </li>
    </ul>
  </nav>