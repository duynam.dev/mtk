@extends('backend.layouts.index')
@section('title','Tạo giao dịch mới')
@section('page-title','Tạo giao dịch mới')
@section('parent-breadcrumb','Giao dịch')
@section('child-breadcrumb','Tạo mới')
@section('content')
	<div class="col-lg-12">
		<form action="{{ route('be.payments.store') }}" method="post">
			 @csrf
		<div class="box box-success">
			<div class="box-header">
			<h3 class="box-title">Tạo giao dịch mới</h3>
			<button class="btn btn-flat btn-success pull-right" type="submit"><i class="fa fa-plus"></i>Lưu</button>
			</div>
			<div class="box-body">
				<div class="form-group">
					<label class="control-label">Đại lý</label>
					<select class="form-control select-2" name="agency_id" id="sel_agency">
						<option value="0">- Chọn đại lý -</option>
						@foreach($agencies as $agency)
							<option value="{{ $agency->id }}">{{ $agency->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label class="control-label">Người thanh toán</label>
					<select class="form-control" name="customer_id" id="sel_customer">
						<option value="0">- Chọn khách hàng -</option>
					</select>
					<span class="help-block" style="color: red;" id="cus_alert"></span>
				</div>
				<div class="form-group">
					<label class="control-label">Amount</label>
					<input type="text" name="amount" class="form-control col-lg-4 money-input" required />
				</div>
				<div class="form-group">
					<label class="control-label">Payment Method</label>
					<select class="form-control col-lg-4" name="category_id">
						@foreach(Config::get('constants.payment_method') as $key => $payment_method)
							<option value="{{ $payment_method }}">{{ $key }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		</form>
	</div>
@endsection

@section('scripts')
	<script type="text/javascript">
		$("#sel_agency").change(function(){
		$('#sel_customer').find('option').remove();
			var agency_id = $(this).val();
			if(agency_id>0)
			{
				$.ajax({
					'url':'{{ route('be.ajax.customer.getbyagency') }}',
					'method': 'post',
					'data':{
						agency_id: agency_id
					},
					success: function(data){

						if(data.status){
							$("#cus_alert").html('');
							$.each(data.customers, function(i, cus){
								$('#sel_customer').append($('<option>', {
        							value: cus.id,
        							text : cus.fullname+' - '+cus.phone_number
    							}));
							});
						}else{
							$("#cus_alert").html('Không tìm thấy người đại diện nào của đại lý này');
						}
					}
				});
			}
		});
	</script>
@endsection