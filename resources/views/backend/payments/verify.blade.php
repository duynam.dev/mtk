@extends('backend.layouts.index') @section('title','Xác nhận giao dịch #'.$payment->id) @section('page-title','Xác nhận giao
dịch #'.$payment->id) @section('parent-breadcrumb','Giao dịch') @section('child-breadcrumb','Xác nhận') @section('content')
<div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <label class="control-label">Đại lý: {{ $payment->agency->name }}</label>

                </div>
                <div class="form-group">
                    <label class="control-label">Người thanh toán: {{ $payment->customer->fullname }}</label>


                </div>
                <div class="form-group">
                    <label class="control-label">SĐT: {{ $payment->customer->phone_number }}</label>


                </div>
                <div class="form-group">
                    <label class="control-label">Số tiền: {{ $payment->amount }}</label>

                </div>
                <div class="form-group">
                    <form class="form-inline" method="POST" action="{{ route('be.payments.verify',$payment) }}">
                        @csrf
                        <div class="form-group mb-2">
                            <label class="control-label">Mã xác nhận:</label>
                            <input name="pin_code" type="text" placeholder="PIN CODE" class="form-control" required>

                        </div>
                        <input type="hidden" name="payment_id" value="{{ $payment->id }}">
                        <button class="btn btn-flat btn-success mb-2" type="submit">
                            <i class="fa fa-plus"></i>Lưu</button>
                    </form>

                </div>
            </div>
        </div>
</div>
@endsection
