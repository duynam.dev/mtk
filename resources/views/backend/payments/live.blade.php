@extends('backend.layouts.index')
@section('title','Giao dịch hôm nay')
@section('page-title','Giao dịch')
@section('parent-breadcrumb','Catalogue')
@section('child-breadcrumb','Giao dịch')
@section('content')

	<div class="col-lg-12">
		<div class="card">
			
			<div class="card-body">
					<h3 class="card-title">Giao dịch hôm nay (Live) <a href="{{ route('be.payments.create') }}"><button class="btn btn-flat btn-success pull-right"><i class="fa fa-plus"></i>Tạo mới</button></a></h3>
					{{-- <leaderboard></leaderboard> --}}
					<livepaymentstable></livepaymentstable>
			</div>
		</div>
    </div>

@endsection
@section('scripts')

<script>

</script> 
@endsection