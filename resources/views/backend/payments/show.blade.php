@extends('backend.layouts.index') @section('title','Thông tin giao dịch #'.$payment->id) @section('page-title','Thông tin giao
dịch #'.$payment->id) @section('parent-breadcrumb','Giao dịch') @section('child-breadcrumb','Thông tin') @section('content')
<div class="col-lg-12">
        <div class="box box-info">
            <div class="box-body">
                <div class="form-group">
                    {{-- <label class="control-label">Đại lý: {{ $payment->agency->name }}</label> --}}

                </div>
                <div class="form-group">
                    <label class="control-label">Người thanh toán: {{ $payment->customer->fullname }}</label>


                </div>
                <div class="form-group">
                    <label class="control-label">SĐT: {{ $payment->customer->phone_number }}</label>


                </div>
                <div class="form-group">
                    <label class="control-label">Số tiền: {{ $payment->amount }}</label>

                </div>
                <div class="form-group mb-2">
                    <label class="control-label">Mã xác nhận: <strong>{{ $payment->verify_code }}</strong></label>
                </div>

                </div>
            </div>
        </div>
</div>
@endsection
