@extends('backend.layouts.index')
@section('title','Giao dịch')
@section('page-title','Giao dịch')
@section('parent-breadcrumb','Catalogue')
@section('child-breadcrumb','Giao dịch')
@section('content')
	<div class="col-lg-12">
		<div class="card">
			
			<div class="card-body">
					<h3 class="card-title">Danh sách các giao dịch <a href="{{ route('be.payments.create') }}"><button class="btn btn-flat btn-success pull-right"><i class="fa fa-plus"></i>Tạo mới</button></a></h3>
					<livepaymentstable></livepaymentstable>
				{{-- <table class="mdl-data-table" id="paymentTable" width="100%">
					<thead>
						<th>#</th>

						<th>Đại lý</th>
						<th>Người thanh toán</th>
						<th>Số tiền</th>
						<th>Trạng thái</th>
						<th>Người tạo</th>
						<th>Thời gian tạo</th>
					</thead>
					<tbody>
						@if(count($payments) > 0)
						@foreach($payments as $payment)
						<tr>
							<td>{{ $payment->id }}</td>

							<td>@if($payment->customer->agency){{ $payment->customer->agency->name }} @else "Không xác định" @endif</td>
							<td>@if($payment->customer){{ $payment->customer->fullname }} @else "Không xác đinh" @endif</td>
							<td>{{ $payment->amount }}</td>
							<td>{!! $payment->status_label !!}</td>
							<td>{{ $payment->user->name }}</td>
							<td><a href="javascript:void(0)" title="{{ date('d/m/Y H:i:s', strtotime($payment->created_at)) }}">{{ \Carbon\Carbon::parse($payment->created_at)->diffForHumans() }}</a></td></td>
						</tr>
						@endforeach
						@else
						<tr>
							<td align="center" colspan="6">Chưa có giao dịch nào!</td>
						</tr>
						@endif
					</tbody>
				</table> --}}
			</div>
		</div>
	</div>
@endsection
@section('scripts')


@endsection