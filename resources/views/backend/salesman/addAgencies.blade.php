@extends('backend.layouts.index')
@section('title','Thêm đại lý cho nhân viên '.$salesman->name)
@section('page-title','Thêm đại lý cho nhân viên '.$salesman->name)
@section('parent-breadcrumb','Nhân viên bán hàng')
@section('child-breadcrumb','Thêm đại lý')
@section('content')
<div class="col-lg-12">
    <form>
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">Thêm đại lý</h3>
                <div class="margin pull-right">
                    <button class="btn btn-flat btn-success =" name="save" type="submit" id="save"><i class="fas fa-save"></i>Lưu</button>
                <button class="btn btn-flat btn-success" name="save-continue" type="submit">
                    <i class="fas fa-save"></i>Lưu và tiếp tục chỉnh sửa</button>
                    <a href="{{ route('be.salesman.index') }}"><button class="btn btn-flat btn-danger" name="cancel" type="button">
                            <i class="fas fa-times"></i>Cancel</button></a>
                </div>

            </div>
            <div class="box-body">                     
                           <div class="box">
                               <div class="box-header">
                               <h3 class="box-title">Danh sách đại lý chưa có Sale phụ trách</h3>
                                </div>
                               <div class="box-body">
                                    <span>Tổng số đại lý nhân viên đang quản lý: </span><span class="label label-success">{{ $salesman->agencies_count }}</span><br>

                                   <span>Tổng số đại lý đã lựa chọn: </span><span class="label label-info" id="selectedCount">0</span>
                                   <div class="filter row">
                                        <div class="col-md-12">
                                        </div>
                                   </div>
                                    <table class="table" id="agenciesTable">
                                        <thead>
                                            <tr>
                                            <th></th>
                                            <th>Mã ĐL</th>
                                            <th>Tên đại lý</th>
                                            <th>Địa chỉ</th>
                                            <th>Xã/Phường</th>
                                            <th>Quận/Huyện</th>
                                            <th>Tỉnh/Thành</th>
                                        </tr>
                                        <tr>
                                                <th></th>
                                                <th>Mã ĐL</th>
                                                <th>Tên đại lý</th>
                                                <th>Địa chỉ</th>
                                                <th>Xã/Phường</th>
                                                <th>Quận/Huyện</th>
                                                <th>Tỉnh/Thành</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                       
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th></th>
                                                <th>Mã ĐL</th>
                                                <th>Tên đại lý</th>
                                                <th>Địa chỉ</th>
                                                <th>Xã/Phường</th>
                                                <th>Quận/Huyện</th>
                                                <th>Tỉnh/Thành</th>
                                            </tr> 
    
                                            </tfoot>
                                    </table>
                                    <button class="btn btn-success">Thêm mới đại lý
                                    </button>
                               </div>
                           </div>
                </div>
        </div>
    </form>
</div>
@endsection
@section('scripts')
<script>
    // $('#agenciesTable thead tr:eq(1) th').each( function (i) {
    //     if(i>0){
    //         var title = $('#agenciesTable thead th').eq( $(this).index() ).text();
    //     $(this).html( '<input type="text" placeholder="Search '+title+'" data-index="'+i+'" />' );
    //     }
        
    // } );
    var table = $("#agenciesTable").DataTable({
        'ajax': "{{ route('api.agency.notHaveSale') }}",
        "processing": true,
        "deferRender": true,
        aoColumns:[
            { "data" : 'id',
              render: function(data, type, row)
              {
                  return '';
              },
              orderable: false,
              className: 'select-checkbox',
            },
            { "data" : "id" },
            { "data" : "name" },
            { "data" : "address" },
            { "data" : "ward.name" },
            { "data" : "district.name" },
            { "data" : "province.name" },
           
        ],
        dom: 'Bfrtip',
        buttons: [  
            {
            extend: 'selectAll',
            text: 'Chọn tất cả',
            action:  function (e) {
                e.preventDefault();
                table.rows({ search: 'applied'}).deselect();
                table.rows({ search: 'applied'}).select()

             }
            },
            // 'selectAll',
            // 'selectNone',
            {
            extend: 'selectNone',
            text: 'Bỏ chọn tất cả',
            // 'selectAll',
            // 'selectNone',
            },
            {
            extend: 'selected',
            text: 'Hiển thị các đại lý đã chọn',
            action: function ( e, dt, button, config ) {
                if (button.text() == 'Hiển thị các đại lý đã chọn') {
                    dt.rows({ selected: false }).nodes().to$().css({"display":"none"});
                    button.text('Hiện tất cả');
                }
                else {
                    dt.rows({ selected: false }).nodes().to$().css({"display":"table-row"});
                    button.text('Hiển thị các đại lý đã chọn');
                }
            }
        }
        ],
           
        select: {
                style:    'multi',
                selector: 'td:first-child'
        },
        initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                if(this.indexes()[0] > 1){
                    var select = $('<select class="form-control"><option value=""></option></select>')
                    .appendTo( $(column.header()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
                }

            } );
        },
    });
    table.on('select',function(e, dt, type, indexes){
        $("#selectedCount").html(table.rows( { selected: true } ).count());
    });
    table.on('deselect',function(e, dt, type, indexes){
        $("#selectedCount").html(table.rows( { selected: true } ).count());
    });
    $("#save").click(function(e){
        e.preventDefault();
        var ids = $.map(table.rows({selected:true}).data(), function (item) {
        return item.id;
        });
        $.ajax({
            url: "{{ route('api.salesman.addAgencies') }}",
            method: 'post',
            data:{
                agency_ids: ids,
                sale_id : '{{ $salesman->id }}'
            },
            success: function(data){
                console.log(data);
            }
        });
    });
</script>
@endsection
