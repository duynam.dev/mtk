@extends('backend.layouts.index')
@section('title','Tạo nhân viên bán hàng mới')
@section('page-title','Tạo nhân viên bán hàng mới')
@section('parent-breadcrumb','Nhân viên bán hàng')
@section('child-breadcrumb','Tạo mới')
@section('content')
<div class="col-lg-12">
    <form action="{{ route('be.salesman.store') }}" class="needs-validation" method="post">
        @csrf

        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">Thêm nhân viên bán hàng</h3>
                <div class="margin pull-right">
                    <button class="btn btn-flat btn-success =" name="save" type="submit"><i class="fas fa-save"></i>Lưu</button>
                <button class="btn btn-flat btn-success" name="save-continue" type="submit">
                    <i class="fas fa-save"></i>Lưu và tiếp tục chỉnh sửa</button>
                    <a href="{{ route('be.salesman.index') }}"><button class="btn btn-flat btn-danger" name="cancel" type="button">
                            <i class="fas fa-times"></i>Cancel</button></a>
                </div>

            </div>
            <div class="box-body">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#info" data-toggle="tab" aria-expanded="true">Thông tin</a>
                        </li>
                        <li>
                            <a href="#agency" data-toggle="tab" aria-expanded="false">Danh sách đại lý</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab tab-pane active" id="info">
                            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                <label class="control-label">Họ và tên</label>
                                <input class="form-control" name="name" value="{{ old('name') }}">
                                @if($errors->has('name'))
                                    <div class="help-block" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </div>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                <label class="control-label">Email</label>
                                <input class="form-control" name="email" type="email" value="{{ old('email') }}" required>
                                 @if($errors->has('email'))
                                    <span class="help-block" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
                                <label class="control-label">SĐT</label>
                                <input class="form-control" name="phone" type="text" value="{{ old('phone') }}" required>
                                 @if($errors->has('phone'))
                                    <span class="help-block" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                                <label class="control-label">Mật khẩu</label>
                                <input class="form-control" name="password" type="password">
                                 @if($errors->has('password'))
                                    <span class="help-block" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="control-label">Mật khẩu</label>
                                <input class="form-control" name="password_confirmation" type="password">
                            </div>
                        </div>
                        <div class="tab tab-pane" id="agency">
                            <p>Bạn cần phải lưu thông tin trước khi thêm đại lý cho nhân viên.</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </form>
</div>
@endsection
