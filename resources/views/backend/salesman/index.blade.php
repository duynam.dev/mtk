@extends('backend.layouts.index')
@section('title','Nhân viên bán hàng')
@section('page-title','Nhân viên bán hàng')
@section('parent-breadcrumb','Catalogue')
@section('child-breadcrumb','Nhân viên bán hàng')
@section('content')
	<div class="col-lg-12">
		<div class="box">
			<div class="box-header">
			<h3 class="box-title">Danh sách</h3>
			<a href="{{ route('be.salesman.create') }}"><button class="btn btn-flat btn-success pull-right"><i class="fa fa-plus"></i>Tạo mới</button></a>
			</div>
			<div class="box-body">
				<table class="mdl-data-table" id="salesmanTale" width="100%">
					<thead>
						<th>Mã NV</th>
						<th>Họ Tên</th>
                        <th>Email</th>
						<th>Số đại lý</th>
						<th>Số giao dịch đã thực hiện</th>
						<th>Doanh só</th>
						<th>Trạng thái</th>
						<th>Ngày tạo</th>
						<th>Action</th>
					</thead>
					<tbody>
						@foreach($salesmen as $salesman)
						<tr>
							<td>{{ $salesman->id }}</td>

							<td>{{ $salesman->name }}</td>
							<td>{{ $salesman->email }}</td>
							
							<td>
								{{ $salesman->agencies_count }}
							</td>
							<td>{{ $salesman->payments_count }}</td>
							<td>
								{!! number_format($salesman->revenue) !!}
							</td>
							<td>{!! $salesman->status_label !!}</td>
							<td><a href="javascript:void(0)" title="{{ date('d/m/Y H:i:s', strtotime($salesman->created_at)) }}">{{ \Carbon\Carbon::parse($salesman->created_at)->diffForHumans() }}</a></td>
							<td><a href="{{ route('be.salesman.edit',$salesman->id) }}"><button class="btn btn-default"><i class="fas fa-edit"></i> Edit</button></a></td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection
@section('scripts')
<script>
	$("#salesmanTale").DataTable({ 


	});
</script>
@endsection