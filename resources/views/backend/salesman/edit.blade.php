@extends('backend.layouts.index')
@section('title','Chỉnh sửa thông tin nhân viên')
@section('page-title','Chỉnh sửa thông tin nhân viên')
@section('parent-breadcrumb','Nhân viên bán hàng')
@section('child-breadcrumb','Chỉnh sửa')
@section('content')
<div class="col-lg-12">
    <form action="{{ route('be.salesman.update',$salesman->id) }}" class="needs-validation" method="post">
            <div class="margin pull-right">
                    <button class="btn btn-flat btn-success =" name="save" type="submit"><i class="fas fa-save"></i>Lưu</button>
                <button class="btn btn-flat btn-success" name="save-continue" type="submit">
                    <i class="fas fa-save"></i>Lưu và tiếp tục chỉnh sửa</button>
                <a href="{{ route('be.salesman.index') }}"><button class="btn btn-flat btn-danger" name="cancel" type="button">
                    <i class="fas fa-times"></i>Cancel</button></a>
                </div>

        @csrf
        @method('patch')
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                       <li class="active">
                           <a href="#statistic" data-toggle="tab" aria-expanded="false">Thống kê</a>
                       </li>
                        <li>
                            <a href="#agency" data-toggle="tab" aria-expanded="false">Danh sách đại lý</a>
                        </li>
                        <li >
                            <a href="#info" data-toggle="tab" aria-expanded="true">Thông tin</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab tab-pane active" id="statistic">
                            <div class="row">
                                    <div class="col-md-8">
                                            <!-- LINE CHART -->
                                            <div class="box box-info">
                                              <div class="box-header with-border">
                                                <h3 class="box-title">Biểu đồ doanh số tháng {!! date('m')  !!}</h3>
                                  
                                                <div class="box-tools pull-right">
                                                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                                  </button>
                                                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                                </div>
                                              </div>
                                              <div class="box-body">
                                                <div class="chart">
                                                  {{-- <canvas id="lineChart" width="400" height="300"></canvas> --}}
                                                  {!! $daychartjs->render() !!}
                                                </div>
                                              </div>
                                              <!-- /.box-body -->
                                            </div>
                                            <!-- /.box -->
                                    </div>
                                    
                            </div>
                            <div class="row">
                                    <div class="col-md-12">
                                            <!-- LINE CHART -->
                                            <div class="box box-info">
                                              <div class="box-header with-border">
                                                <h3 class="box-title">Biểu đồ doanh số năm {!! date('Y')  !!}</h3>
                                  
                                                <div class="box-tools pull-right">
                                                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                                  </button>
                                                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                                </div>
                                              </div>
                                              <div class="box-body">
                                                <div class="chart">
                                                  {{-- <canvas id="lineChart" width="400" height="300"></canvas> --}}
                                                  {!! $yearchartjs->render() !!}
                                                </div>
                                              </div>
                                              <!-- /.box-body -->
                                            </div>
                                            <!-- /.box -->
                                    </div>
                                    
                            </div>
                            
                                        
                        </div>
                        <div class="tab tab-pane" id="info">
                            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                <label class="control-label">Họ và tên</label>
                                <input class="form-control" name="name" value="{{ $salesman->name }}">
                                @if($errors->has('name'))
                                    <div class="help-block" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </div>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                <label class="control-label">Email</label>
                                <input class="form-control" name="email" type="email" value="{{ $salesman->email }}" required>
                                 @if($errors->has('email'))
                                    <span class="help-block" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
                                <label class="control-label">SĐT</label>
                                <input class="form-control" name="phone" type="text" value="{{ $salesman->phone }}" required>
                                 @if($errors->has('phone'))
                                    <span class="help-block" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="tab tab-pane" id="agency">                       
                           <div class="box">
                               <h3 class="box-title">Danh sách đại lý</h3>
                               <div class="box-body">
                                    <table class="mdl-data-table" style="width:100%" id="agenciesTable">
                                        <thead>
                                            <th>Mã ĐL</th>
                                            <th>Tên đại lý</th>
                                            <th>Địa chỉ</th>
                                            <th>Xã/Phường</th>
                                            <th>Quận/Huyện</th>
                                            <th>Tỉnh/Thành</th>
                                            <th></th>
                                        </thead>
                                        <tbody>
                                          
                                        </tbody>
                                    </table>
                                    <a href="{{ route('be.salesman.addAgencies',$salesman->id) }}"><button class="btn btn-success"  type="button">Thêm mới đại lý
                                    </button></a>
                               </div>
                           </div>
                        </div>
                    </div>
                </div>

    </form>
</div>
@endsection
@section('scripts')
    <script>
        var table = $("#agenciesTable").DataTable({
            ajax: "{{ route('api.salesman.getAgencies',$salesman->id) }}",
            "processing": true,
            "deferRender": true,
            columns:[
         
            { "data" : "id" },
            { "data" : "name" },
            { "data" : "address" },
            { "data" : "ward.name" },
            { "data" : "district.name" },
            { "data" : "province.name" },
            { "data" : "id",
              render: function(data, row, index){
                  return '<button class="btn btn-danger del_agency" data-id="'+data+'" type="button">Xóa</button>';
              }
            }
            ],
        });
        $("#agenciesTable").on('click','.del_agency', function(){
            var agency_id = $(this).attr('data-id');
            swal({
                title: "Xóa đại lý",
                text: "Bạn muốn xóa đại lý này khỏi danh sách quản lý của nhân viên?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                })
                .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: '{{ route("api.salesman.delAgency") }}',
                        method: 'post',
                        data:{
                            agency_id: agency_id,
                        },
                        success: function(data){
                            if(data){
                                swal(" Xóa thành công", {
                    icon: "success",
                    });
                    table.ajax.reload();
                            }
                           
                        }
                    });
                    
                }
            });
        });
    </script>
@endsection
