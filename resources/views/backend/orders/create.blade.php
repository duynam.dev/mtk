@extends('backend.layouts.index')
@section('title','Create Order')
@section('page-title','Create Order')
@section('parent-breadcrumb','Order')
@section('child-breadcrumb','Create')
@section('content')
	<div class="col-lg-12">
		<form action="{{ route('be.orders.store') }}" method="post">
			 @csrf
		<div class="card">
			<div class="card-title">
			Create Order
			<button class="btn btn-flat btn-success pull-right" type="submit"><i class="fa fa-plus"></i>Save</button>
			</div>
			<div class="card-body">
				<div class="form-group">
					<label class="control-label">Code</label>
					<input type="text" name="code" class="form-control col-lg-4" required />
				</div>
				<div class="form-group">
					<label class="control-label">Customer</label>
					<select name="customer_id" class="form-control">
						@foreach($customers as $customer)
							<option value="{{ $customer->id }}">{{ $customer->fullname }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label class="control-label">Item</label>
					<select class="form-control" name="item_id">
						@foreach($items as $item)
							<option value="{{ $item->id }}">{{ $item->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label class="control-label">Quantity</label>
					<input type="number" name="quantity" class="form-control col-lg-4" required />
				</div>
				
			</div>
		</div>
		</form>
	</div>
@endsection