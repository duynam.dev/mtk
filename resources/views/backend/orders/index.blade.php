@extends('backend.layouts.index')
@section('title','Orders')
@section('page-title','Orders')
@section('parent-breadcrumb','Catalogue')
@section('child-breadcrumb','Order')
@section('content')
	<div class="col-lg-12">
		<div class="card">
			<div class="card-title">
			List
			<a href="{{ route('be.orders.create') }}"><button class="btn btn-flat btn-success pull-right"><i class="fa fa-plus"></i>Add New</button></a>
			</div>
			<div class="card-body">
				<table class="table">
					<thead>
						<th>#</th>
						<th>Code</th>
						<th>Item</th>						
						<th>Customer</th>						
						<th>Quantity</th>
						<th>Amount</th>
						<th>Status</th>
						<th>Action</th>
					</thead>
					<tbody>
						@foreach($orders as $order)
						<tr>
							<td>{{ $order->id }}</td>
							<td>{{ $order->code }}</td>
							<td>{{ $order->item->name }}</td>
							<td>{{ $order->customer->fullname }}</td>
							<td>{{ $order->quantity }}</td>
							<td>{{ $order->amount }}</td>
							<td><span class="badge badge-{{ $order->status_label }}">{{ $order->status }}</span></td>
							<td>
								<a href="{{ route('be.orders.edit',$order) }}"><button class="btn btn-flat"><i class="fa fa-pencil"></i> Edit</button></a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection