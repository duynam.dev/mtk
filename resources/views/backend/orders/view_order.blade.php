@extends('backend.layouts.index')
@section('title','View Order')
@section('page-title','View Order - '.$order->code)
@section('parent-breadcrumb','Order')
@section('child-breadcrumb','Create')
@section('content')

	<div class="col-lg-12">
		<form action="{{ route('be.orders.update',$order) }}" method="post" width="100%">
			 @csrf
		<div class="card">
			<div class="card-title">
			View Order <span class="text-primary">{{ $order->code }}</span>
			<button class="btn btn-flat btn-success pull-right" type="submit"><i class="fa fa-plus"></i>Save</button>
			</div>
			<div class="card-body">
				<div class="form-group">
					<label class="control-label">Code</label>
					<input type="text" name="code" class="form-control col-lg-4" value="{{ $order->code }}" required />
				</div>
				<div class="form-group">
					<label class="control-label">Customer</label>
					<select name="customer_id" class="form-control">
						@foreach($customers as $customer)
							<option value="{{ $customer->id }}" @if($order->customer_id == $customer->id) selected="true" @endif>{{ $customer->fullname }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label class="control-label">Item</label>
					<select class="form-control" name="item_id" id="sel_item">
						@foreach($items as $item)
							<option value="{{ $item->id }}" @if($order->item_id == $item->id) selected="true" @endif>{{ $item->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label class="control-label">Quantity</label>
					<input type="number" name="quantity" class="form-control col-lg-4" value="{{ $order->quantity }}" id="quantity" required />
				</div>
				<span class="text-left">Total: </span><span class="pull-right font-weight-bold" id="total_amount">{{ ($order->quantity)*($order->item->price) }}</span>

			</div>
		</div>

	</div>
	<div class="col-lg-12">
		<div class="card">
			<div class="card-title">
				<span>Payments</span>
				<button class="btn btn-success pull-right" type="button" data-toggle="modal" data-target="#payment_modal">Add Payment</button>
			</div>
			<div class="card-body">
				<table class="table">
					<thead>
						<th>#</th>
						<th>Payer Name</th>
						<th>Amount</th>
						<th>Type</th>
						<th>Created Date</th>
						<th>Status</th>
					</thead>
					<tbody>
						@if(count($order->payments) == 0)
							<tr>
								<td colspan="5" class="text-center">No Payments Found!</td>
							</tr>
						@endif
						@foreach($order->payments as $payment)
							<tr>
								<td>{{ $payment->id }}</td>
								<td>{{ $payment->person_pay_name }}</td>
								<td>{{ $payment->amount }}</td>
								<td>{{ $payment->type }}</td>
								<td>{{ $payment->created_at }}</td>
								<td>@if($payment->paid_date != null) <span class="badge badge-success">Paid</span> @else <span class="badge badge-default">Pending</span> @endif</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		</form>
	</div>
<div class="modal" tabindex="-1" role="dialog" id="payment_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Add Payment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{ route('be.payments.store') }}" method="post">
       @csrf
      <input type="hidden" name="order_id" value="{{ $order->id }}">
      <div class="modal-body">
        <div class="form-group row">
    <label for="staticEmail" class="col-sm-2 col-form-label">Order:</label>
    <div class="col-sm-10">
      <input type="text" readonly class="form-control-plaintext" id="staticOrder" value="{{ $order->code }}">
    </div>
  </div>
    <div class="form-group row">
    <label for="staticEmail" class="col-sm-2 col-form-label">Payer</label>
    <div class="col-sm-10">
      <select class="form-control" id="sel_customer" name="customer_id">
      	<option value="0">Other</option>
      	@foreach($customers as $customer)
      		<option value="{{ $customer->id }}" @if($customer->id == $order->customer->id) selected="true" @endif>{{ $customer->fullname }} - {{ $customer->phone_number }}</option>
      	@endforeach
      </select>
    </div>
  </div>
    <div class="form-group row">
    <label for="payer_name" class="col-sm-2 col-form-label">Payer Name</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="person_pay_name" id="payer_name" value="{{ $order->customer->fullname }}">
    </div>
  </div>
    <div class="form-group row">
    <label for="staticEmail" class="col-sm-2 col-form-label">Verify Phone:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="verify_phone_number" id="verify_phone_number" value="{{ $order->customer->phone_number }}">
    </div>
  </div>
      <div class="form-group row">
    <label for="staticEmail" class="col-sm-2 col-form-label">Amount:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="amount" id="amount">
    </div>
  </div>

      <div class="form-group row">
    <label for="staticEmail" class="col-sm-2 col-form-label">Payment Method</label>
    <div class="col-sm-10">
     <select class="form-control" name="payment_method">
     	@foreach(Config::get('constants.payment_method') as $key => $value)
     		<option value="{{ $value }}"><span class="text-uppercase">{{ $key }}</span></option>
     	@endforeach
     </select>
    </div>
  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>

@endsection
@section('scripts')

	<script type="text/javascript">
		var APP = {};
		var APP = {

			API: {
				getItem : '{{ route('be.ajax.item.get') }}',
				getCustomer: '{{ route('be.ajax.customer.get') }}'
			}
		}
		APP.getItem = function(id){
			$.ajax({
				'type': 'get',
				'url' : APP.API.getItem,
				'data' : {
					id: id
				},
				success: function(data){
					APP.updateTotal(data.item.price);
				}
			})
		}
		APP.getCustomer = function(id){
			$.ajax({
				'type' : 'get',
				'url' : APP.API.getCustomer,
				'data' : {
					id : id
				},
				success: function(data){
					APP.setPhoneAndName(data.customer.fullname, data.customer.phone_number);
				}
			})
		}
		APP.updateTotal = function(price){
			var qty = $("#quantity").val()*price;
			$("#total_amount").html(qty);
		}
		APP.setPhoneAndName = function(name, phone)
		{
			$("#payer_name").val(name);
			$("#verify_phone_number").val(phone);
		}
		$(document).ready(function(){
			$("#sel_item").change(function(){
				var id = $(this).val();
				APP.getItem(id);
			});
			$("#quantity").keyup(function(){
				var id = $("#sel_item").val();
				APP.getItem(id);
			});
			$("#sel_customer").change(function(){
				var id = $(this).val();
				if(id != 0){
					APP.getCustomer(id);
				}else{
					APP.setPhoneAndName('','');
				}

			});
		});

	</script>
@endsection