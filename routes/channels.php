<?php
use App\Models\Eloquent\Payment;
/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('payment.created.{id}', function ($user, $user_id) {
    if($user->isAdmin()){
        return true;
    }
    return (int) $user->id === (int) $user_id;
});
Broadcast::channel('admin', function ($user) {
    if($user->isAdmin()){
        return true;
    }
    return false;
   
});