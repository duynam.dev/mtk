	<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::get('/import', 'Backend\AgencyController@import');
Route::get('/', 'HomeController@index');
Route::get('/sms', 'Backend\SMSController@index');
// Route::get('/t',function(){
//     var_dump(Auth::user()->isAdmin());
// });
// Route::get('/pusher', function() {
//     event(new App\Events\HelloPusherEvent('Hi there Pusher!'));
//     return "Event has been sent!";
// });
// Route::get('/incomingsms', ['uses' => 'Backend\PaymentController@webHook']);
// Route::get('/sendsms',['uses' => 'Backend\PaymentController@sendSMS']);
// Route::get('/vue', function(){
//     return view('layouts.index');
// });
// Route::get('/', function(){
//     return view('index');
// });
// Route::group([
//     'as' => 'be.',
//     'namespace' => 'Backend',
//     'middleware' => ['web','auth']
// ], function(){

//     Route::get('/payments/live', function(){
//         return view('backend.payments.live');
//     })->name('payments.live');
//     /* CATEGORY ROUTES */

//     Route::group([
//         'as' => 'category.',
//         'prefix' => 'category',
//         'middleware' => ['web']
//     ], function(){
//         Route::get('/', ['as' => 'index', 'uses' => 'CategoryController@index']);
//         Route::get('/create',['as' => 'create', 'uses' => 'CategoryController@createForm']);
//         Route::post('/store',['as' => 'store', 'uses' => 'CategoryController@store']);
//         Route::get('/update/{id}',['as' => 'update', 'uses' => 'CategoryController@updateForm']);
//         Route::post('/save',['as' => 'save', 'uses' => 'CategoryController@save']);
//         Route::post('/delete',['as' => 'delete', 'uses' => 'CategoryController@delete']);
//         Route::get('/webhook/{id}',['as' => 'webhook', 'uses' => 'CategoryController@testWebHook']);
//     });
//     Route::resources(
//         [
//             'items' => 'ItemController',
//             'orders' => 'OrderController',
//             'customers' => 'CustomerController',
//             'payments' => 'PaymentController',
//             'agency' => 'AgencyController',
//             'users' => 'UserController',
//             'salesman' => 'SalesmanController'
//         ]
//     );
//     Route::get('/salesman/{saleman}/add-agencies',['as' => 'salesman.addAgencies', 'uses' => 'SalesmanController@addAgencies']);
//     Route::post('/payments/verify/{id}',['as' => 'payments.verify', 'uses' => 'PaymentController@pinVerify']);
//     Route::get('/payments/verify/{id}',['as' => 'payments.verify.input', 'uses' => 'PaymentController@showVerifyForm']);
//     Route::post('/payments/confirm',['as' => 'payments.confirm','uses' => 'PaymentController@confirm']);
//     Route::get('/sms-history', ['as' => 'sms.history', 'uses' => 'SMSController@index']);
//     Route::group([
//         'as' => 'ajax.',
//         'prefix' => 'ajax',
//         'middleware' => ['web']
//     ], function(){
//         Route::group([
//             'as' => 'item.',
//             'prefix' => 'item'
//         ], function(){
//             Route::get('/get', ['as' => 'get', 'uses' => 'ItemController@ajaxGetItem']);
//         });
//         Route::group([
//             'as' => 'customer.',
//             'prefix' => 'customer'
//         ], function(){
//             Route::get('/get', ['as' => 'get', 'uses' => 'CustomerController@ajaxGetCustomer']);
//             Route::post('/get-by-agency',['as' => 'getbyagency', 'uses' => 'CustomerController@ajaxGetCustomerByAgency']);
//         });
//     });
//     //PROVINCE, WARD, DISTRICT AJAX ROUTES
//     Route::group([
//         'as' => 'address.'
//     ],function(){
//         Route::post('/districts-by-province',['as' => 'district.byProvince','uses' => 'AddressController@getDistrictsByProvinceId']);
//         Route::post('/wards-by-district',['as' => 'ward.byDistrict','uses' => 'AddressController@getWardsByDistrictId']);
//     });
// });

//Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
