<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'namespace' => 'API',
    'middleware' => 'api',
], function () {
    Route::get('/', function () {
        return 'test';
    });
    Route::group([
        'as' => 'test.',
        'prefix' => 'test',
    ], function () {
        Route::get('/', ['uses' => 'SMSLogController@getAccountInfo']);
    });
    Route::post('/auth/login', 'AuthController@login');
    Route::delete('/auth/logout', 'AuthController@logout');
    Route::group(['middleware' => 'jwt.auth'], function () {
        //
        Route::group([
            'as' => 'summary.',
            'prefix' => 'summary',
        ], function () {
            Route::get('/payment-by-day', ['uses' => 'PaymentController@getCountPaymentByDays']);
            Route::get('/dashboard', ['uses' => 'DashboardController@getSummary']);
        });
        Route::group([
            'prefix' => 'payment',
            'as' => 'payment.',
        ], function () {
            Route::get('/live', ['as' => 'live', 'uses' => 'PaymentController@getLive']);
            Route::get('/', ['as' => 'all', 'uses' => 'PaymentController@getAll']);
            Route::post('/store', ['as' => 'store', 'uses' => 'PaymentController@store']);
            Route::post('/update', ['as' => 'store', 'uses' => 'PaymentController@update']);
            Route::post('/confirm', ['as' => 'store', 'uses' => 'PaymentController@confirm']);
            Route::post('/destroy', ['as' => 'destroy', 'uses' => 'PaymentController@destroy']);
            Route::post('/deletePaymentDetail', ['as' => 'deletePaymentDetail', 'uses' => 'PaymentController@deletePaymentDetail']);
            Route::post('/editPaymentDetail', ['as' => 'editPaymentDetail', 'uses' => 'PaymentController@editPaymentDetail']);
        });
        Route::group([
            'prefix' => 'agency',
            'as' => 'agency.',
        ], function () {
            Route::get('/', ['as' => 'get', 'uses' => 'AgencyController@getAll']);
            Route::get('/no-sale', ['as' => 'notHaveSale', 'uses' => 'AgencyController@getAgenciesNotHaveSale']);
            Route::post('/delete', ['as' => 'agency.delete', 'uses' => 'AgencyController@delete']);
            Route::post('/', ['as' => 'get', 'uses' => 'AgencyController@getById']);
            Route::post('/store', ['as' => 'store', 'uses' => 'AgencyController@store']);
            Route::post('/update', ['as' => 'update', 'uses' => 'AgencyController@update']);
        });

        Route::post('auth/user', 'AuthController@get');

        Route::group([
            'as' => 'salesman.',
            'prefix' => 'salesman',
        ], function () {
            Route::get('/', ['as' => 'get', 'uses' => 'SalesmanController@get']);
            Route::post('/', ['as' => 'getById', 'uses' => 'SalesmanController@getById']);
            Route::post('/update', ['as' => 'update', 'uses' => 'SalesmanController@update']);
            Route::post('/sync-agencies', ['as' => 'syncAgencies', 'uses' => 'SalesmanController@syncAgencies']);
            Route::post('/salesman/add-agencies', ['as' => 'salesman.addAgencies', 'uses' => 'SalesmanController@addAgencies']);
            Route::post('/salesman/delete-agencies', ['as' => 'salesman.delAgency', 'uses' => 'SalesmanController@deleteAgency']);
        });
        Route::group([
            'as' => 'user.',
            'prefix' => 'user',
        ], function () {
            Route::get('/', ['as' => 'getAll', 'uses' => 'UserController@getAll']);
            Route::post('/store', ['as' => 'store', 'uses' => 'UserController@store']);
            Route::post('/toggleActive', ['as' => 'toggleActive', 'uses' => 'UserController@toggleActive']);
            Route::post('/resetpassword', ['as' => 'resetPassword', 'uses' => 'UserController@resetPassword']);
        });
        Route::group([
            'as' => 'sms.',
            'prefix' => 'sms',
        ], function () {
            Route::get('/', ['as' => 'getAll', 'uses' => 'SMSLogController@getAll']);
            Route::get('/info', ['as' => 'getInfo', 'uses' => 'SMSLogController@getAccountInfo']);
        });
        Route::group([
            'as' => 'role.',
            'prefix' => 'role',
        ], function () {
            Route::get('/', ['as' => 'getAll', 'uses' => 'RoleController@getAll']);
        });
        Route::get('/provinces', ['uses' => 'AddressController@provinces']);
        Route::post('/provinces/districts', ['uses' => 'AddressController@getDistrictsByProvinceId']);
        Route::post('/districts/wards', ['uses' => 'AddressController@getWardsByDistrictId']);
    });

});
