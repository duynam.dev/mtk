<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->integer('item_id');
            $table->integer('quantity');
            $table->enum('status',['PENDING','PROCESSING','COMPLETE','CANCELED']);
            $table->integer('customer_id');
            $table->string('shipping_address');
            $table->dateTime('shipped_datetime');
            $table->dateTime('completed_datetime');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
