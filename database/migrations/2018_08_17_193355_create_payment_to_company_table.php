<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentToCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_to_company', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payment_id');
            $table->integer('user_paid_id');
            $table->integer('user_confirm_id');
            $table->integer('type');
            $table->integer('reference_code')->nullable();
            $table->string('note');
            $table->integer('amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_to_company');
    }
}
