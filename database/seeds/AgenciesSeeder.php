<?php

use Illuminate\Database\Seeder;

class AgenciesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $names = array([
    'ĐẠI LÝ SÁU THƯƠNG',
    'ĐẠI LÝ MINH MƯỜI',
    'ĐẠI LÝ HÙNG LONG',
    'ĐẠI LÝ HÙNG HẠNH',
    'ĐẠI LÝ THÀNH CÔNG',
    'ĐẠI LÝ TƯ ĐÔNG',
    'ĐẠI LÝ Y NGUYÊN',
    'ĐẠI LÝ HOA NAM',
    'ĐẠI LÝ THUỶ KM28',
    'ĐẠI LÝ  HTX DV NN HÀM CHÍNH 1',
    'ĐẠI LÝ  HOÀNG NGHI',
    'ĐẠI LÝ  NGỌC ĐẠT',
    'ĐẠI LÝ  HAI  LIÊM',
    'ĐẠI LÝ  ANH ĐỨC',
    'ĐẠI LÝ  MỸ HẰNG',
    'ĐẠI LÝ PHÂN BÓN PHUƠNG TỰ',
    'KHÁT VỌNG XANH',
    'ĐẠI LÝ  HƯNG THẠNH',
    'ĐẠI LÝ  NGỌC GIÀU',
    'ĐẠI LÝ  HẢI VIỆT',
    'ĐẠI LÝ  ĐỒNG PHÁT 2',
    'ĐẠI LÝ  CHÍN  NGA',
    'ĐẠI LÝ  ĐỒNG PHÁT 3',
    'ĐẠI LÝ  CƯỜNG MAI',
    'SIÊU THỊ NHÀ NÔNG',
    'ĐẠI LÝ BÌNH PHƯỚC 2',
    'ĐẠI LÝ NGUYỄN NHẬT NAM',
    'ĐẠI LÝ PHONG PHÚ',
    'ĐẠI LÝ THẢO TRANG',
    'ĐẠI LÝ THU HÀ',
    'ĐẠI LÝ HỒNG SƠN',
    'ĐẠI LÝ ĐỒNG PHÁT 4',
    'Siêu Thị Nhà Nông Lộc Ninh',
    'Đại Lý NGỌC HOAN',
    'Đại Lý Nguyễn Trung',
    'Đại Lý QUANG PHƯƠNG',
    'Đại Lý Thành Thúy',
    'Đại Lý MINH ĐỨC',
    'Đại Lý TRUNG HIẾU',
    'ĐẠI LÝ THẢO NHƠN',
    'ĐẠI LÝ GIA HƯNG',
    'ĐẠI LÝ THÙY TRANG',
    'ĐẠI LÝ SƠN HẢI',
    'ĐẠI LÝ  -SƯƠNG PHÁT',
    'CỬA HÀNG NĂM VINH',
    'ĐẠI LÝ CÔNG HÀ',
    'ĐẠI LÝ  MINH VỸ',
    ' ĐẠI LÝ  TRÂM OÁNH',
    ' ĐẠI LÝ MINH HÂN',
    ' ĐẠI LÝ  - LE THỊ KIM LAN',
    'ĐẠI LÝ  - TRẦN LÊN',
    'ĐẠI LÝ  -ÁNH HỒNG 2',
    'ĐẠI LÝ  - ĐỨC MẠNH',
    'ĐẠI LÝ  -THANH HÀ 11',
    'ĐẠI LÝ  -THANH HẢI',
    'ĐẠI LÝ  -GIANG LONG THẮNG',
    'ĐẠI LÝ  -Phân Bón  Thuốc Bảo Vệ Thực Vật An Nguyên',
    'ĐẠI LÝ THÀNH TÂM - NGUYỄN TẤT THÀNH',
    'ĐẠI LÝ TRANG THỊNH',
    'ĐẠI LÝ  HÙNG CƯỜNG',
    'ĐẠI LÝ Tùng Dung',
    'Dai ly Ba Phước',
    'Dai ly Chiến Thảo',
    'ĐẠI LÝ DUY LAN -  LÊ THỊ LAN',
    'ĐẠI LÝ QUÝ MÃO',
    'ĐẠI LÝ THỦY SỸ -  LÊ THỊ THỦY',
    'Đại lý HỒNG MINH',
    'Đại lý HỘI QUY',
    'Đại lý HẰNG ANH',
    'Đại lý QUỐC  ĐẠT - TRẦN THỊ LOÃN',
    'Đại lý ÂN NGOAN',
    'CỬA HÀNG DUNG SÁNG',
    'CỬA HÀNG YẾN ĐÃI',
    'CỬA HÀNG DUY NGHIỄM',
    'CỬA HÀNG ANH VŨ',
    'ĐẠI LÝ QUANG PHƯƠNG',
    'ĐẠI LÝ  - LONG LiỄU',
    'Kính gửi: ĐẠI LÝ  - HỒNG SƠN',
    'ĐẠI LÝ  -HAI PHI',
    'ĐẠI LÝ  -ĐỨC THẢO',
    'ĐẠI LÝ  -CHIỀU VUI',
    'ĐẠI LÝ  -Kỷ Thảo',
    'ĐẠI LÝ  -THÚY KHÁNH',
    'ĐẠI LÝ  -DŨNG THANH 1',
    'ĐẠI LÝ  -DŨNG THANH 2',
    'ĐẠI LÝ  -MINH HiẾU',
    'ĐẠI LÝ  -TÂM LINH',
    'ĐẠI LÝ  -MẠNH TOAN',
    'ĐẠI LÝ  -BÍCH GIÁM',
    'ĐẠI LÝ  -HiẾU QUYÊN',
    'ĐẠI LÝ  -HÙNG NGỌC',
    'ĐẠI LÝ  -HẠNH PHONG',
    'ĐẠI LÝ  -Hùng Cường  72',
    'Kính gửi: :ĐẠI LÝ  Phước Hồng',
    'ĐẠI LÝ TRUC NGHIA',
    'Kính gửi: :ĐẠI LÝ  HOÀN HOA',
    'CÔNG TY TNHH PHÂN BÓN MAI HƯƠNG',
    'DNTN TM Minh Đào ',
    'ĐẠI LÝ KIM HẬU- TÔ NỮ THANH VÂN',
    'ĐẠI LÝ MAI NGUYÊN',
    'NGUYEN THI CAM',
    'Dai Ly Thảo Xuân',
    'ĐẠI LÝ TUẤN THÚY',
    'ĐẠI LÝ ĐỨC ANH - PHẠM THỊ TUYẾT',
    ]);
    public function run()
    {
        $wards = DB::table('wards')
            ->select('wards.*', 'provinces.id', 'districts.id')
            ->join('districts', 'wards.district_id', '=', 'districts.id')
            ->join('provinces', 'provinces.id', '=', 'districts.province_id')	
			->whereIn('provinces.id',[79,80])
            ->get();
        foreach($this->names as $name)
        {
            $ward = $wards->random();
            DB::table('agency')->insert([
                'name' => $name,
                'province_id' => 79,
                'district_id' => $ward->district_id,
                'ward_id'     => $ward->id,
                'address'     => str_random(20)
            ]);
        }

    }
}
