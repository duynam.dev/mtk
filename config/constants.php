<?php
	return [
		'order_status' => [
			'canceled' => 0,
			'pending'  => 1,
			'processing' => 2,
			'completed' => 3
		],
		'payment_method' => [
			'bank' => 1,
			'cash' => 2
		],
		'payment_status' => [
			
			'sms_not_sent' => 1,
			'pending' => 2,
			'processing' => 3,
			'completed' => 4,
			'cancelled' => 0,
		],
		'user_status' => [
			'active' => 1,
			'banned' => 2,
			'inactive' => 0,
		]
	];
?>