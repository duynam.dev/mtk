
import Pusher from 'pusher-js'
import Echo from 'laravel-echo'
import {config} from '../config/'
export const socket = {
  pusher: null,
  channel: null,
  echo: null,
  async init () {
    return new Promise((resolve) => {
   
      this.pusher = new Pusher('6b952ee0b40139ef921f', {
        authEndpoint: config.base_url+'/broadcasting/auth',
        auth: {
          headers: {
            Authorization: `Bearer`+ localStorage.getItem('token')
          }
        },
        cluster: 'ap1',
        encrypted: true
      });

    //   this.echo = new Echo({
    //     broadcaster: 'pusher',
    //     authEndpoint: `http://mtk.test/api/broadcasting/auth`,
    //     key: '6b952ee0b40139ef921f',
    //     cluster: 'ap1',
    //     encrypted: true,
    //     auth: {
    //         headers: {
    //             Authorization: `Bearer` +localStorage.getItem('token')
    //         }
    //     }
    // });

      this.channel = this.pusher.subscribe('private-admin')
      this.channel.bind('pusher:subscription_succeeded', () => {
        return resolve()
      })
      this.channel.bind('pusher:subscription_error', () => {
        return resolve()
      })
  


    })
  },

  /**
   * Broadcast an event with Pusher.
   * @param  {string} eventName The event's name
   * @param  {Object} data      The event's data
   */
  // broadcast (eventName, data = {}) {
  //   this.channel && this.channel.trigger(`client-${eventName}.${userStore.current.id}`, data)

  //   return this
  // },

  // /**
  //  * Listen to an event.
  //  * @param  {string}   eventName The event's name
  //  * @param  {Function} cb
  //  */
  listen (eventName, cb) {
    
    this.channel && this.channel.bind(`App\\Events\\${eventName}`, data => cb(data))
  
    return this
  }
}
