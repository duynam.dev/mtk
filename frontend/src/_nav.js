import store from './stores/store'

export default {
  items: [
    {
      name: 'Tổng quan',
      url: '/dashboard',
      icon: 'icon-speedometer',
      // badge: {
      //   variant: 'primary',
      //   text: 'NEW'
      // }
    },
    {
      title: true,
      name: 'KHÁCH HÀNG',
      class: 'text-success',
      wrapper: {
        element: '',
        attributes: {}
      }
    },
    {
      name: 'Đại lý',
      url: '/agency',
      icon: 'icon-home'
    },
    // {
    //   name: 'Typography',
    //   url: '/theme/typography',
    //   icon: 'icon-pencil'
    // },
    {
      title: true,
      name: 'GIAO DỊCH',
      class: 'text-success',
      wrapper: {
        element: '',
        attributes: {}
      }
    },
    
    {
      name: 'Hôm nay',
      url: '/payment-live',
      icon: 'icon-graph',
      class: store.getters.isAdmin == 1 ? 'text-success' : 'd-none',
      // badge: { 
      //   variant: 'success',
      //   text: 'LIVE'
      // }
    },
    {
      name: 'Lịch sử',
      url: '/payment',
      icon: 'icon-calendar',
    },
    {
      title: true,
      name: 'USER',
      class: store.getters.isAdmin == 1 ? 'text-success' : 'd-none',
      wrapper: {
        element: '',
        attributes: {}
      }
    },
    {
      name: 'Nhân viên giao dịch',
      url: '/salesman',
      icon: 'fa fa-address-card',
      class: store.getters.isAdmin == 1 ? 'text-success' : 'd-none',
    },
    {
      name: 'Nguời dùng',
      url: '/users',
      icon: 'fa fa-users',
      class: store.getters.isAdmin == 1 ? 'text-success' : 'd-none',
    },
        {
      title: true,
      name: 'SMS',
      class: store.getters.isAdmin == 1 ? 'text-success' : 'd-none',
      wrapper: {
        element: '',
        attributes: {}
      }
    },
    {
      name: 'Lịch sử tin nhắn',
      url: '/sms',
      icon: 'fa fa-envelope-o',
      class: store.getters.isAdmin == 1 ? 'text-success' : 'd-none',
    },

    // {
    //   name: 'Administrator',
    //   url: '/user',
    //   icon: 'icon-calendar',
    //   class: store.getters.isAdmin == 1 ? 'text-success' : 'd-none',
    // },
    // {
    //   name: 'Buttons',
    //   url: '/buttons',
    //   icon: 'icon-cursor',
    //   children: [
    //     {
    //       name: 'Buttons',
    //       url: '/buttons/standard-buttons',
    //       icon: 'icon-cursor'
    //     },
    //     {
    //       name: 'Button Dropdowns',
    //       url: '/buttons/dropdowns',
    //       icon: 'icon-cursor'
    //     },
    //     {
    //       name: 'Button Groups',
    //       url: '/buttons/button-groups',
    //       icon: 'icon-cursor'
    //     },
    //     {
    //       name: 'Brand Buttons',
    //       url: '/buttons/brand-buttons',
    //       icon: 'icon-cursor'
    //     }
    //   ]
    // },
    // {
    //   name: 'Charts',
    //   url: '/charts',
    //   icon: 'icon-pie-chart'
    // },
    // {
    //   name: 'Icons',
    //   url: '/icons',
    //   icon: 'icon-star',
    //   children: [
    //     {
    //       name: 'CoreUI Icons',
    //       url: '/icons/coreui-icons',
    //       icon: 'icon-star',
    //       badge: {
    //         variant: 'info',
    //         text: 'NEW'
    //       }
    //     },
    //     {
    //       name: 'Flags',
    //       url: '/icons/flags',
    //       icon: 'icon-star'
    //     },
    //     {
    //       name: 'Font Awesome',
    //       url: '/icons/font-awesome',
    //       icon: 'icon-star',
    //       badge: {
    //         variant: 'secondary',
    //         text: '4.7'
    //       }
    //     },
    //     {
    //       name: 'Simple Line Icons',
    //       url: '/icons/simple-line-icons',
    //       icon: 'icon-star'
    //     }
    //   ]
    // },
    // {
    //   name: 'Notifications',
    //   url: '/notifications',
    //   icon: 'icon-bell',
    //   children: [
    //     {
    //       name: 'Alerts',
    //       url: '/notifications/alerts',
    //       icon: 'icon-bell'
    //     },
    //     {
    //       name: 'Badges',
    //       url: '/notifications/badges',
    //       icon: 'icon-bell'
    //     },
    //     {
    //       name: 'Modals',
    //       url: '/notifications/modals',
    //       icon: 'icon-bell'
    //     }
    //   ]
    // },
    // {
    //   name: 'Widgets',
    //   url: '/widgets',
    //   icon: 'icon-calculator',
    //   badge: {
    //     variant: 'primary',
    //     text: 'NEW'
    //   }
    // },
    // {
    //   divider: true
    // },
    // {
    //   title: true,
    //   name: 'Extras'
    // },
    // {
    //   name: 'Pages',
    //   url: '/pages',
    //   icon: 'icon-star',
    //   children: [
    //     {
    //       name: 'Login',
    //       url: '/pages/login',
    //       icon: 'icon-star'
    //     },
    //     {
    //       name: 'Register',
    //       url: '/pages/register',
    //       icon: 'icon-star'
    //     },
    //     {
    //       name: 'Error 404',
    //       url: '/pages/404',
    //       icon: 'icon-star'
    //     },
    //     {
    //       name: 'Error 500',
    //       url: '/pages/500',
    //       icon: 'icon-star'
    //     }
    //   ]
    // },
    // {
    //   name: 'Download CoreUI',
    //   url: 'http://coreui.io/vue/',
    //   icon: 'icon-cloud-download',
    //   class: 'mt-auto',
    //   variant: 'success'
    // },
    // {
    //   name: 'Try CoreUI PRO',
    //   url: 'http://coreui.io/pro/vue/',
    //   icon: 'icon-layers',
    //   variant: 'danger'
    // }
  ]
}
