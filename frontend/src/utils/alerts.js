import swal from 'sweetalert2';

const alerts = {
    alert(msg) {
        swal(msg);
    },
    success(msg, okFunc = null){
        swal({
            title: "Thành công!",
            text: msg,
            type: "success",
            confirmButtonText: "OK",
        }).then((result)=>{
            if(result.value && okFunc !== null){
                okFunc();
            }
        });
    },
    error(msg){
        swal({
            title: "Thất bại!",
            text: msg,
            type: "error",
            confirmButtonText: "OK",
        });
    },
    warning(msg){
        swal({
            title: "Thất bại!",
            text: msg,
            type: "warning",
            confirmButtonText: "OK",
        });
    },
    ajaxConfirm(title,type = 'info', func, okFunc = null){
        swal({
            title: title,
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'Cancel',
            showLoaderOnConfirm: true,
            type: type,
            preConfirm: () => {
              return func()
                .then(response => {
                  if (!response.status) {

                    throw new Error(response.msg)
                  }
                  return response
                })
                .catch(error => {
                   
                  swal.showValidationError(error)
                })
            },
            allowOutsideClick: () => !swal.isLoading()
          }).then((result) => {
              //console.log(result)
            if (typeof result.value !== 'undefined' && typeof result.value.status !== 'undefined') {
                if(result.value.status){
                    this.success(result.value.msg, okFunc);
                }else{
                      swal.showValidationError(result.value.msg);
                }
             
             
            }
          }).then(()=>{
              if(okFunc && typeof okFunc === "function")
              {
              okFunc();
              }

          })
    },
    ajaxRequest(title, param, okFunc){
        swal({
            title: title,
            input: 'text',
            inputAttributes: {
              autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'Xác nhận',
            showLoaderOnConfirm: true,
            preConfirm: (login) => {
                okFunc(param, login);

            },
            allowOutsideClick: () => !swal.isLoading()
          })
    }
}
export { alerts };