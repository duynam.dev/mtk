
import Vue from 'vue';
import VueX from 'vuex';
import axios from 'axios'
import {event} from '@/utils'
import {userStore} from './user'
import {http} from '@/services'
import VueCookies from 'vue-cookie'

Vue.use(VueCookies)
Vue.use(VueX);

export default new VueX.Store({
  state: {
    status: '',
    token: localStorage.getItem('token') || '',
    user: JSON.parse(VueCookies.get('user')),
    is_admin: localStorage.getItem('is_admin') || '',
  },
  mutations: {
    auth_request(state){
      state.status = 'loading'
    },
    auth_success(state, payload){
      state.status = 'success'
      state.token = payload.token
      state.user = payload.user
      state.is_admin = payload.is_admin
    },
    auth_error(state){
      state.status = 'error'
    },
    logout(state){
      state.status = ''
      state.token = ''
    },
  },
  actions: {
    login({commit}, credentials){
      return new Promise((resolve, reject) => {
        commit('auth_request')
        http.post('auth/login', {email: credentials.email, password: credentials.password},(resp) => {
          const token = resp.data.token
          VueCookies.set('user',JSON.stringify(resp.data.user),60*60*30)
          //let is_admin = resp.data.user.is_admin
          localStorage.setItem('token', token)
          localStorage.setItem('is_admin',resp.data.user.is_admin)
          axios.defaults.headers.common['Authorization'] = token
          event.emit('auth:login')
          commit('auth_success', {token: token, user:resp.data.user, is_admin: resp.data.user.is_admin})
          resolve(resp)
        }, (err) => {
          reject(err.response)
        })
       
      })
      // .catch((err) => {
      //     commit('auth_error')
      //     localStorage.removeItem('token')
      //     reject(err.response)
      //   })
  },
    logout({ commit }) {
      localStorage.removeItem("token");
      event.emit('logout')
      commit('logout');
    }
  },
	getters : {
  isLoggedIn: state => !!state.token,
  authStatus: state => state.status,
  isAdmin: state => state.is_admin
	}
});