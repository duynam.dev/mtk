import Vue from 'vue'
import Router from 'vue-router'
import store from '../stores/store'
// Containers
const DefaultContainer = () => import('@/containers/DefaultContainer')

// Views
const DashboardExample = () => import('@/views/Dashboard')

// const Colors = () => import('@/views/theme/Colors')
// const Typography = () => import('@/views/theme/Typography')

// const Charts = () => import('@/views/Charts')
// const Widgets = () => import('@/views/Widgets')

// // Views - Components
// const Cards = () => import('@/views/base/Cards')
// const Forms = () => import('@/views/base/Forms')
// const Switches = () => import('@/views/base/Switches')
// const Tables = () => import('@/views/base/Tables')
// const Tabs = () => import('@/views/base/Tabs')
// const Breadcrumbs = () => import('@/views/base/Breadcrumbs')
// const Carousels = () => import('@/views/base/Carousels')
// const Collapses = () => import('@/views/base/Collapses')
// const Jumbotrons = () => import('@/views/base/Jumbotrons')
// const ListGroups = () => import('@/views/base/ListGroups')
// const Navs = () => import('@/views/base/Navs')
// const Navbars = () => import('@/views/base/Navbars')
// const Paginations = () => import('@/views/base/Paginations')
// const Popovers = () => import('@/views/base/Popovers')
// const ProgressBars = () => import('@/views/base/ProgressBars')
// const Tooltips = () => import('@/views/base/Tooltips')

// // Views - Buttons
// const StandardButtons = () => import('@/views/buttons/StandardButtons')
// const ButtonGroups = () => import('@/views/buttons/ButtonGroups')
// const Dropdowns = () => import('@/views/buttons/Dropdowns')
// const BrandButtons = () => import('@/views/buttons/BrandButtons')

// Views - Icons
// const Flags = () => import('@/views/icons/Flags')
// const FontAwesome = () => import('@/views/icons/FontAwesome')
// const SimpleLineIcons = () => import('@/views/icons/SimpleLineIcons')
// const CoreUIIcons = () => import('@/views/icons/CoreUIIcons')

// Views - Notifications
// const Alerts = () => import('@/views/notifications/Alerts')
// const Badges = () => import('@/views/notifications/Badges')
// const Modals = () => import('@/views/notifications/Modals')

// Views - Pages
const Page404 = () => import('@/views/pages/Page404')
const Page500 = () => import('@/views/pages/Page500')
const Login = () => import('@/views/pages/Login')
// const Register = () => import('@/views/pages/Register')

// Users
// const Users = () => import('@/views/users/Users')
const UserProfile = () => import('@/views/users/User')
const Dashboard = () => import('@/components/dashboard')
const Agency = () => import('@/components/agency/')
const CreateAgency = () => import('@/components/agency/create')
const EditAgency = () => import('@/components/agency/edit')
const PaymentsTable = () => import('../components/payments/PaymentsTable')
const LivePaymentsTable = () => import('../components/payments/LivePaymentsTable')
const Salesman = () => import('../components/salesman/Salesman.vue')
const EditSalesman = () => import('../components/salesman/edit.vue')
const Users = () => import('../components/user/')
const CreateUser = () => import('../components/user/create')
const SMSLogs = () => import('../components/sms')
Vue.use(Router)


const router = new Router({
  mode: 'hash', // https://router.vuejs.org/api/#mode
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '/',
      redirect: '/dashboard',
      name: 'Trang chủ',
      component: DefaultContainer,
      meta:{
        requiresAuth: true
      },
      children: [
        {
          path: 'dashboard',
          name: 'Tổng quan',
          component: Dashboard,
          meta:{
            requiresAuth: true
          },
        },
        {
          path: '/agency',
          name: 'Đại lý',
          redirect: '/agency/list',
          component: {
            render (c) { return c('router-view') }
          },
          meta:{
            requiresAuth: true
          },
          children:[
            {
              path: 'list',
              name: '',
              component: Agency,
              meta:{
                requiresAuth: true
              },
            },
            {
              path: 'create',
              name: 'Tạo mới',
              component: CreateAgency,
              meta:{
                requiresAuth: true
              },
            },
            {
              path: 'edit/:agencyId',
              name: 'Cập nhật đại lý',
              component: EditAgency,
              meta:{
                requiresAuth: true
              },
            },
          ]
        },

        {
          path: 'payment',
          name: 'Lịch sử giao dịch',
          component: PaymentsTable,
          meta:{
            requiresAuth: true
          },
        },
        {
          path: 'payment-live',
          name: 'Giao dịch hôm nay',
          component: LivePaymentsTable,
          meta:{
            requiresAuth: true
          },
        },
        {
          path: '/salesman',
          redirect: '/salesman/list',
          name: 'Nhân viên giao dịch',
          component: {
            render (c) { return c('router-view') }
          },
          meta:{
            requiresAuth: true
          },
          children:[
            {
              path: 'list',
              name: '',
              component: Salesman,
              meta:{
                requiresAuth: true
              },
            },
            {
              path: 'edit/:saleId',
              name: 'Cập nhật nhân viên',
              component: EditSalesman,
              meta:{
                requiresAuth: true
              },
            }
          ]
        },
        {
          path: '/users',
          redirect: '/users/list',
          name: 'Người dùng',
          component: {
            render (c) { return c('router-view') }
          },
          meta:{
            requiresAuth: true
          },
          children:[
            {
              path: 'list',
              name: '',
              component: Users,
              meta:{
                requiresAuth: true
              },
            },
            {
              path: 'create',
              name: 'Tạo mới người dùng',
              component: CreateUser,
              meta:{
                requiresAuth: true
              },
            },
            // {
            //   path: 'edit/:saleId',
            //   name: 'Cập nhật nhân viên',
            //   component: EditSalesman,
            //   meta:{
            //     requiresAuth: true
            //   },
            // }
          ]
        },
        {
          path: 'sms',
          name: 'SMS',
          component: SMSLogs,
          meta:{
            requiresAuth: true
          },
        },
      ]
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/pages',
      redirect: '/pages/404',
      name: 'Pages',
      component: {
        render (c) { return c('router-view') }
      },
      children: [
        {
          path: '404',
          name: 'Page404',
          component: Page404
        },
        {
          path: '500',
          name: 'Page500',
          component: Page500
        },
                {
          path: 'profile',
          name: 'Profile',
          component: UserProfile
        },
      ]
    }
  ]
})
router.beforeEach((to, from, next) => {

  if(to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.isLoggedIn) {
      next()
      return
    }
    next('/login') 
  } else {
    next() 
  }
})

export default router;
