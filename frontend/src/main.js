// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'core-js/es6/promise'
import 'core-js/es6/string'
import 'core-js/es7/array'

// import cssVars from 'css-vars-ponyfill'
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App' 
import router from './router'
import Notifications from 'vue-notification'

import axios from 'axios';
import VueX from 'vuex';
import VueAxios from 'vue-axios';
import {http} from '@/services'
import store from './stores/store'
import {event} from '@/utils'
import 'axios-progress-bar/dist/nprogress.css';
import {config} from './config'
import vi from 'vee-validate/dist/locale/vi';
import VeeValidate, { Validator }  from 'vee-validate';
import Icon from 'vue-awesome/components/Icon'
import 'vue-awesome/icons'
const moment = require('moment')
require('moment/locale/vi')
Vue.use(require('vue-moment'), {
    moment
})
Vue.use(Notifications)
Vue.use(VueX)
Vue.use(VueAxios, axios)
Validator.localize('vi',vi)
const validateconfig = {
  aria: true,
  classNames: {},
  classes: false,
  delay: 0,
  dictionary: null,
  errorBagName: 'errors', // change if property conflicts
  events: 'input|blur',
  fieldsBagName: 'formfields',
  i18n: null, // the vue-i18n plugin instance
  i18nRootKey: 'validations', // the nested key under which the validation messages will be located
  inject: true,
  locale: 'vi',
  strict: true,
  validity: false,
};
Vue.use(VeeValidate,validateconfig)
Vue.axios.defaults.baseURL = config.base_url;


Vue.config.devtools = true
Vue.mixin({
  methods: {
  comma: function (Num) {
    Num += '';
    Num = Num.replace(',', ''); Num = Num.replace(',', ''); Num = Num.replace(',', '');
    Num = Num.replace(',', ''); Num = Num.replace(',', ''); Num = Num.replace(',', '');
    let x = Num.split('.');
    let x1 = x[0];
    let x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1))
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    return x1 + x2;
  }
  }
  });
// Vue.config.devtools = true;
// Vue Auth

// todo
// cssVars()
Vue.router = router 
Vue.use(BootstrapVue)

Vue.component('v-icon',Icon)
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App),
  created () {
    event.init()
    http.init()
  },
  store
});
